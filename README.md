# Serverless System Architecture

## Author: Abrar H Galib

## Version: 1.0.0

## Last Updated: 02/09/2021

### System Architecture

![System Architecture](./docs/arch-version-1.0.0.png)

### Client Applications

There are two types of interfaces offered to the user at this point:

1. Windows Server native applications that will be developed as a full management and distribution system for tasks.

2. WhatsApp Mobile interface for allowing users to interact with a simple chat bot

I have spent some time developing the WhatsApp interface. The flow of data has been described below:

1. User opts in for receiving messages on the Service Number.

2. User sends a message. Message format:
    command [… options | keywords]

3. User identity is verified. For now, only employees are searched.
    1. If an employee with number exists in database, only then user message is processed

    2. Else an error message is sent to the user

    3. Based on the command, a response is sent to the user. The response format is application/xml.

Check the interface documentation guide for a list of supported commands and response.

The Windows application will access the system via the REST API. API Gateway provides the access for client application to send HTTP requests based for data. The Lambda Authorizer implemented validates the requests by first, verifying the tokens passed in the HTTP header and then checking for appropriate content access as defined by the permissions.json file that is deployed as part of the authorizer function.

Client applications can also subscribe to SQS group/channel for updates on new task creation. The polling time should be chosen such that it does not overwhelm the system. Right now, SQS is setup with default settings. The maximum message retention period is 4 days. There is no dead lettering setup, so error while polling might lead to message losses. Once a message has been polled, it will be destroyed. The Deduplication Key for the message is the randomly generated task ID. Each message is a simple JSON payload (check Message Schema guide for more information).

> **Notes on random IDs**
> IDs must be generated such that they are unique for each entity in the database. This will avoid key collision. Right now, no automated ID creation is used for DynamoDB. Each partition key is therefore, a randomly generated string. IDs are created using RFC version 4 (random) UUID specification.

### API Gateway

The gateway provides the main interface for all incoming HTTP traffic. The API is setup as Lambda Proxy and divided among 3 Lambda functions that expose a bare bone Express REST API. A Lambda Request authorizer is implemented that controls access to the API via generated access policies. The mechanism is described below:

1. Once a user is logged into a session using Cognito in the Windows application, Cognito will issue three tokens:
    - ID Token

    - Access Token

    - Refresh Token

2. These tokens must then be cached in the application local store. Each HTTP request will attach these tokens in their headers with the following names:
    - X-Cognito-ID-Token: <Cognito_ID_Token_Value>

    - X-Cognito-Access-Token: <Cognito_Access_Token_Value>

3. For every request that tries to access an API endpoint, the Lambda authorizer will check headers for these values. If an access token is invalid, then it will be attempted to be replaced with new token. If token refresh fails, an authorized 403 status will be sent from the API Gateway. A detailed outline of the authorization workflow is added subsequent section of this document

4. Once a request successfully passes the authentication check, the request will be redirected to the Lambda proxy endpoint

For client side application, note the following:

- The custom header names should be lowercase in case the API responds with 500 with a null message body. This forced lowercase conversion is not needed when making the request through a web application. Postman API caused the above reported issue

- A mock API called test-interface has been developed as a guide for the REST API client. It can be used as a reference

- Refresh token must be saved and used to refresh the authentication token on client side. The current validity for ID and Access tokens are set at one hour (60 minutes)

- When tokens are received, two user attributes should be saved besides other necessary attributes returned by Cognito user pool: custom:role and custom:company_id. The role values can either be admin, agency_admin or employee. The company_id for employee refers to the company they are registered with while for agency_admin it refers to the agency they are registered with

### Lambda Layer

The Lambda exposes a REST API that is developed using ExpressJS. The are two primary functions supported by the Lambda at this point:

1. Serving as a Webhook server for responding the WhatsApp incoming messages on the service number. The Webhook requests are from Twilio WhatsApp application

2. Serving as a REST API that provides access to employee, company and task management

Based on the above functions, the API endpoints are divided into three broad paths, each having a separate Express server deployed on a Lambda function:

1. /messages/*: All messages from Webhook are handled by an endpoint at this path

2. /api/*: All API requests are handled at this path

3. /admin/*: All requests for admin features

There are some extra Lambda triggers that will be deployed for performing background sync for the DynamoDB database. The following triggers will be used:

1. employeeCreated: A Cognito Post Sign-up confirmation trigger to create an entry for the either agency or an employee in DynamoDB. Based on user type, appropriate Role will be assigned. If an employee is added to an agency’s existing company then, quota for employees per company for the target company will be adjusted

2. companyCreated: A DynamoDB trigger for companies table. Each time a new company is created, the agencies company quota will be adjusted

3. monthlyTaskQuotaRenewal: A Lambda function that will be run on 12 am of the 1st day of the month using CloudWatch. This will reset all the monthly quotas for employee task access

4. userRequestCreated: For adjusting the quota limit for task access for the accessed task. A user_request log is created on task access via WhatsApp interface

### S3 Layer

All tasks created by a company will be added to appropriate S3 bucket. Right now, there is only one S3 bucket, company-tasks. This bucket contains folders where, each folder name is the company ID from companies table and represents a single folder for that company. All company task files are added to these company folders.

### Entity Relation Diagram (ERD)

![Entity Relation Diagram (ERD)](./docs/erd.png)

### Database Schema

#### Employee Table

| Field Name      | Field Type | Description |
|-----------------|------------|-------------|
| employee_id     | String     | Hash Key    |
| first_name      | String     | -           |
| last_name       | String     | -           |
| whatsapp_number | String     | GSI         |
| company_id      | String     | GSI         |
| monthly_quota   | Number     | -           |

#### Company Table

| Field Name      | Field Type | Description |
|-----------------|------------|-------------|
| company_id      | String     | Hash Key    |
| company_name    | String     | -           |
| agency_id       | String     | GSI         |
| employees_added | Number     | -           |
| short_code      | String     | GSI         |

#### Task Table

| Field Name     | Field Type    | Description |
|----------------|---------------|-------------|
| task_id        | String        | Hash Key    |
| company_id     | String        | GSI         |
| query_id       | String        | GSI         |
| file_mime_type | String        | -           |
| file_name      | String        | -           |
| frequency      | Number        | -           |
| name           | String        | -           |
| task_number    | Number        | -           |
| keywords       | Array[String] | -           |
| is_live        | Boolean       | -           |

#### Agency Table

| Field Name                | Field Type | Description |
|---------------------------|------------|-------------|
| agency_id                 | String     | Hash Key    |
| agency_name               | String     | -           |
| companies_added           | Number     | -           |
| employees_added           | Number     | -           |
| companies_quota_limit     | Number     | -           |
| employees_quota_limit     | Number     | -           |
| agency_admin_email        | String     | -           |
| agency_admin_phone_number | String     | -           |

#### Query Table

| Field Name   | Field Type | Description |
|--------------|------------|-------------|
| query_id     | String     | Hash Key    |
| db_config_id | String     | GSI         |
| company_id   | String     | GSI         |
| query_name   | String     | -           |
| query_string | String     | -           |

#### DbConfig Table

| Field Name            | Field Type | Description |
|-----------------------|------------|-------------|
| db_config_id          | String     | Hash Key    |
| db_name               | String     | -           |
| company_id            | String     | GSI         |
|  db_connection_string | String     | -           |
| db_connection_url     | String     | -           |

#### Log Table

| Field Name           | Field Type | Description |
|----------------------|------------|-------------|
| log_id               | String     | Hash Key    |
| user_whatsapp_number | String     | GSI         |
| user_role            | String     | -           |
| creation_date_time   | String     | -           |
| request_endpoint     | String     | -           |

#### WhatsAppLog Table

| Field Name           | Field Type | Description |
|----------------------|------------|-------------|
| log_id               | String     | Hash Key    |
| user_whatsapp_number | String     | GSI         |
| message             | String      | -           |
| creation_date_time   | String     | -           |

#### Request Table

| Field Name           | Field Type    | Description |
|----------------------|---------------|-------------|
| request_id           | String        | Hash Key    |
| user_whatsapp_number | String        |             |
| user_id              | String        | GSI         |
| creation_date_time   | String        | -           |
| task_id              | String        | GSI         |
| keywords_used        | Array[String] | -           |
| has_match            | Boolean       | -           |
| is_live              | Boolean       | -           |
| is_completed         | Boolean       | -           |
| completed_date_time  | String        | -           |
