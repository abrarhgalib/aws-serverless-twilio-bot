Sample CRUD using curl
----------------------

Base-Url: https://ycwgd6l03i.execute-api.eu-central-1.amazonaws.com/dev/
Twilio Webhooks: Base-Url/messages
Api: Base-Url/api

1. Get a company by ID
----------------------

Req:
curl -v -X GET https://ycwgd6l03i.execute-api.eu-central-1.amazonaws.com/dev/api/company/f7f39855-7f89-4473-b73d-a586ce81b67b

Res:
{"status":200,"company":{"Item":{"company_id":"f7f39855-7f89-4473-b73d-a586ce81b67b","company_name":"Tyrell Corporation"}}}

2. Get an employee by ID
------------------------

Req:
curl -v -X GET https://ycwgd6l03i.execute-api.eu-central-1.amazonaws.com/dev/api/employee/f37cd37e-0d9d-48be-929e-7b9c71ce4527

Res:
{"status":200,"employee":{"Item":{"company_id":"f7f39855-7f89-4473-b73d-a586ce81b67b","last_name":"Pippin","monthly_quota":2,"employee_id":"f37cd37e-0d9d-48be-929e-7b9c71ce4527","whatsapp_number":"+8801847090622","first_name":"Derek"}}}

3. Get a task by ID
--------------------

Req:
curl -v -X GET https://ycwgd6l03i.execute-api.eu-central-1.amazonaws.com/dev/api/task/ea2898ad-0b1d-4eb8-b7d9-a531a50d2d4b

Res:
{"status":200,"task":{"Item":{"task_number":1,"keywords":["Daily","Outputs","Productivity"],"company_id":"f7f39855-7f89-4473-b73d-a586ce81b67b","task_id":"ea2898ad-0b1d-4eb8-b7d9-a531a50d2d4b","file_name":"DailyOutputs_12-10-2020.pdf","name":"Daily Outputs"}}}


4. Creating a company
---------------------

Req:
curl -v -X PUT \
-H 'Content-Type: application/json' \
-d '{"company_name": "Broadcom Media","agency_id":"4d2978a0-7630-4284-8fc4-535d93cce25a"}' \
https://ycwgd6l03i.execute-api.eu-central-1.amazonaws.com/dev/api/company

Res:
{"status":200,"company_id":"d67b0f83-ecc6-4598-8410-931ac5df0c0c"}


5. Creating an employee
-----------------------

Req:
curl -v -X PUT \
-H 'Content-Type: application/json' \
-d '{"first_name": "Moss","last_name":"Hendricks","whatsapp_number":"+8801847090622","company_id":"d67b0f83-ecc6-4598-8410-931ac5df0c0c","email":"abrarcalculas@gmail.com"}' \
https://ycwgd6l03i.execute-api.eu-central-1.amazonaws.com/dev/api/employee

Res:
{"employee":{"employee_id":"024afcbb-49fd-4923-a567-5356ecc05936","first_name":"Moss","last_name":"Hendricks","whatsapp_number":"+8801847090622","company_id":"d67b0f83-ecc6-4598-8410-931ac5df0c0c","monthly_quota":5,"username":"742933fc-2e01-4429-8df4-ec4b3aacc790"}}


6. Creating a task
------------------

Note: pay close attention to the form fields; the ordering must be like this;
the file must be the final form input; otherwise, it causes undefined behavior
in the Lambda function. Reference: https://stackoverflow.com/questions/39589022/node-js-multer-and-req-body-empty/43197040

Req:
curl -v -X PUT \
-H 'Content-Type: multipart/form-data' \
-F 'company_id=d67b0f83-ecc6-4598-8410-931ac5df0c0c' \
-F 'keywords[]=Monthly' \
-F 'keywords[]=Productivity' \
-F 'name=MonthlyOutputs' \
-F 'task_number=1' \
-F 'query_id=08b0e7ee-9227-41c7-8655-41ed5b47e117' \
-F 'frequency=1' \
-F 'file=@/home/zerocool/freelancer/serverless-api/crud.txt' \
https://kjl3fd1a21.execute-api.eu-central-1.amazonaws.com/dev/api/task

Res:
{"status":200,"task_id":"f0fad1e1-06e8-467c-8599-4ecab04074b8"}


7. Creating an Agency
-------------------------

Req:

curl -v -X PUT \
-H 'Content-Type: application/json' \
-d '{"name": "Pied Piper Inc.", "agency_admin_email":"abrarcalculas@gmail.com", "agency_admin_phone_number":"+8801847090622"}' \
https://ycwgd6l03i.execute-api.eu-central-1.amazonaws.com/dev/admin/agency

Res:
Status: 201
Body: Agency Created


8. Creating an Agency Admin User
--------------------------------

Req:

curl -v -X PUT \
-H 'Content-Type: application/json' \
-d '{"first_name":"Bertram","last_name":"Gilfoyle","whatsapp_number":"+8801711549548","email":"abrarcalculas@gmail.com"}' \
https://ycwgd6l03i.execute-api.eu-central-1.amazonaws.com/dev/admin/agency/21d57461-97b3-4da9-9a0e-836fbcbab350/admin

Res:

{"username":"5374f861-cac1-4963-aeef-4b485a9417ba","attributes":[{"Name":"sub","Value":"5374f861-cac1-4963-aeef-4b485a9417ba"},{"Name":"phone_number","Value":"+8801847090622"},{"Name":"given_name","Value":"Bertram"},{"Name":"family_name","Value":"Gilfoyle"},{"Name":"custom:role","Value":"agency_admin"},{"Name":"email","Value":"abrarcalculas@gmail.com"},{"Name":"custom:company_id","Value":"21d57461-97b3-4da9-9a0e-836fbcbab350"}]}


9. Get the details of an Agency
-------------------------------

Req:

curl -v -X GET \
-H "X-Cognito-ID-Token: 1234" \
-H "X-Cognito-Access-Token: 1234" \
https://ycwgd6l03i.execute-api.eu-central-1.amazonaws.com/dev/admin/agency/21d57461-97b3-4da9-9a0e-836fbcbab350

Res:

{"agency":{"agency_id":"21d57461-97b3-4da9-9a0e-836fbcbab350","agency_admin_email":"abrarcalculas@gmail.com","name":"Pied Piper Inc.","agency_admin_phone_number":"+8801847090622"}}


10. Get all agencies registered
-------------------------------

Req:

curl -v -X GET \
https://ycwgd6l03i.execute-api.eu-central-1.amazonaws.com/dev/admin/agencies

Res:

{"agencies":[{"agency_id":"21d57461-97b3-4da9-9a0e-836fbcbab350","agency_admin_email":"abrarcalculas@gmail.com","name":"Pied Piper Inc.","agency_admin_phone_number":"+8801847090622"}]}


11. Delete an existing agency
-----------------------------

Req:

curl -v -X DELETE \
https://ycwgd6l03i.execute-api.eu-central-1.amazonaws.com/dev/admin/agency/21d57461-97b3-4da9-9a0e-836fbcbab350




{
  username: 'ddc33f36-8ffc-449b-8808-f60a20ca2f1c',
  attributes: [
    { Name: 'sub', Value: 'ddc33f36-8ffc-449b-8808-f60a20ca2f1c' },
    { Name: 'phone_number', Value: '+8801847090622' },
    { Name: 'given_name', Value: 'Abrar' },
    { Name: 'family_name', Value: 'Galib' },
    { Name: 'custom:role', Value: 'agency_admin' },
    { Name: 'email', Value: 'abrarcalculas@gmail.com' },
    {
      Name: 'custom:company_id',
      Value: 'bfbbb667-a972-490d-aba4-c4ed561a4fb3'
    }
  ]
}



curl -v -X POST \
-H 'Content-Type: application/x-www-form-urlencoded' \
-d 'From=whatsapp:%2B8801847090622&To=whatsapp:%2B14715005050&SmsStatus=received&Body=S 900' \
https://ycwgd6l03i.execute-api.eu-central-1.amazonaws.com/dev/messages/incoming

curl -v -X POST \
-H 'Content-Type: application/x-www-form-urlencoded' \
-d 'From=whatsapp:%2B8801847090622&To=whatsapp:%2B14715005050&SmsStatus=received&Body=T%20Daily%20Output' \
https://ycwgd6l03i.execute-api.eu-central-1.amazonaws.com/dev/messages/incoming





authorizer:
                name: api-authorizer
                identitySource: method.request.header.X-Cognito-ID-Token, method.request.header.X-Cognito-Access-Token
                type: request


curl -v -X GET \
-H 'X-Cognito-ID-Token: eyJraWQiOiJJd0V3M3N0N0NJQVlxb1ZhcEMrTm9hZTZmQjYzczVtYmdlTDlFQzQ1Y2ljPSIsImFsZyI6IlJTMjU2In0.eyJzdWIiOiJhN2YxNzg2NS0wYTg0LTQ2NjItOTY0Mi1jYTFjMWZhMmFkNjYiLCJpc3MiOiJodHRwczpcL1wvY29nbml0by1pZHAuZXUtY2VudHJhbC0xLmFtYXpvbmF3cy5jb21cL2V1LWNlbnRyYWwtMV90R1Y0V0pmUjYiLCJjb2duaXRvOnVzZXJuYW1lIjoiYTdmMTc4NjUtMGE4NC00NjYyLTk2NDItY2ExYzFmYTJhZDY2IiwiZ2l2ZW5fbmFtZSI6IkFicmFyIiwiY3VzdG9tOmNvbXBhbnlfaWQiOiI0ZDI5NzhhMC03NjMwLTQyODQtOGZjNC01MzVkOTNjY2UyNWEiLCJhdWQiOiI0aDdocGxqYjkwdG1pNXNqMmM2ajU4azRjdCIsImV2ZW50X2lkIjoiNTcxYmRkMTItZmVhZi00MzFjLWJjNTktNGM3OGE3NTQ2ZWY3IiwidG9rZW5fdXNlIjoiaWQiLCJhdXRoX3RpbWUiOjE2MDk3NzA1MjksInBob25lX251bWJlciI6Iis4ODAxNzExNTQ5NTQ4IiwiZXhwIjoxNjA5Nzc0MTI5LCJjdXN0b206cm9sZSI6ImFnZW5jeV9hZG1pbiIsImlhdCI6MTYwOTc3MDUyOSwiZmFtaWx5X25hbWUiOiJHYWxpYiIsImVtYWlsIjoiYWJyYXJjYWxjdWxhc0BnbWFpbC5jb20ifQ.DbKS3gP41B7jKIE0CgXFQncEwWRtlZHCeLbYoTmU9_dw2GSglRHAqfyxuYp04pDXGPndJHhZY_8wkvL-3DpvhV3tW7t6-E5F29H_92u0TBVpSD5-fNTNTZhre-Q0E55LU2S_puzh7kkWo0mFdHfCm4Q7CHEyZvCNlHaxpaMfAzV5mm0RdWSQyfaArRpR6m78E399MYi24s9kqHGvxsed9ZL1KoJ8lDUCuLCFHqvHficYj1fcnmzE8cFGHcmZzcQPQCknrrIe5wGsbPtllgc3cx91PfOE8Ekon62SDOhrJo1DBzVAem0i8J0JsFXrNWZ_30NF7vEQhkim70FPyV2tKw' \
-H 'X-Cognito-Access-Token: eyJraWQiOiJteU5NZVRGWVQwZ2h6VjNiR3R3ZzVSeG9XaXNxb0JjUWFGdHU2eXQ2dWE0PSIsImFsZyI6IlJTMjU2In0.eyJzdWIiOiJhN2YxNzg2NS0wYTg0LTQ2NjItOTY0Mi1jYTFjMWZhMmFkNjYiLCJldmVudF9pZCI6IjU3MWJkZDEyLWZlYWYtNDMxYy1iYzU5LTRjNzhhNzU0NmVmNyIsInRva2VuX3VzZSI6ImFjY2VzcyIsInNjb3BlIjoiYXdzLmNvZ25pdG8uc2lnbmluLnVzZXIuYWRtaW4iLCJhdXRoX3RpbWUiOjE2MDk3NzA1MjksImlzcyI6Imh0dHBzOlwvXC9jb2duaXRvLWlkcC5ldS1jZW50cmFsLTEuYW1hem9uYXdzLmNvbVwvZXUtY2VudHJhbC0xX3RHVjRXSmZSNiIsImV4cCI6MTYwOTc3NDEyOSwiaWF0IjoxNjA5NzcwNTI5LCJqdGkiOiIwNjZjNGI4MC02N2RlLTRjYzMtOGU0Mi00ZjdhMmY0YjkwODUiLCJjbGllbnRfaWQiOiI0aDdocGxqYjkwdG1pNXNqMmM2ajU4azRjdCIsInVzZXJuYW1lIjoiYTdmMTc4NjUtMGE4NC00NjYyLTk2NDItY2ExYzFmYTJhZDY2In0.bjfaKQeIzHg9x7agq0lvGiBSleH1ARO2EXWS6xXO7rkxuvmeluOCVDr1CiQg3ATcvRc_AF82NuKLKSjYvwrcnXs_TPIHh4bLeIClU3Pmqyz3AjjFfO8Z-N7cvoMvzxpnD_cGMeUmDMS0SEIG3K8qQPBaIhzfiMFt4enIAuLHQy00rcPUuZv1p5_nJ8RHofpVcb-nfMKM-KqxRlVqMQNC9SmyOSYcgDlxrtHeJxbSKdWA6DcdFD0j7sMzkYra40vVSJRfdyWuPFxPzxUlgAI8weXUQWEYuyOzxvvqDJEqNzew0jTKKpp9mumFHaBiudLBhhNnYkhDzfHCU3Dphw3mlg' \
https://kjl3fd1a21.execute-api.eu-central-1.amazonaws.com/dev/api/company/8d71bfac-ac1b-4bbd-9c06-4b1a08077626




curl -v -X PUT \
-H 'Content-Type: application/json' \
-d '{"first_name":"Bertram","last_name":"Gilfoyle","whatsapp_number":"+8801711549548","email":"abrarcalculas@gmail.com"}' \
https://kjl3fd1a21.execute-api.eu-central-1.amazonaws.com/dev/admin/agency/4d2978a0-7630-4284-8fc4-535d93cce25a/admin




curl -v -X PUT \
-H 'Content-Type: application/json' \
-d '{"first_name": "Jane","last_name":"Barber","whatsapp_number":"+17097715025","company_id":"099e597f-5ecc-48c8-a153-42b26a95a350","email":"johnpatrick.jonael@usweek.net"}' \
https://kjl3fd1a21.execute-api.eu-central-1.amazonaws.com/dev/api/employee

curl -v -X PUT \
-H 'Content-Type: application/json' \
-d '{"company_name": "Broadcom Hong Kong","agency_id":"4d2978a0-7630-4284-8fc4-535d93cce25a"}' \
https://kjl3fd1a21.execute-api.eu-central-1.amazonaws.com/dev/api/company



curl -v -X PUT \
-H 'Content-Type: application/json' \
-d '{"first_name": "Bill","last_name":"Crouse","whatsapp_number":"+17097715039","company_id":"099e597f-5ecc-48c8-a153-42b26a95a350","email":"luccas.kazuki@usweek.net"}' \
https://kjl3fd1a21.execute-api.eu-central-1.amazonaws.com/dev/api/employee

curl -v -X PUT \
-H 'Content-Type: application/json' \
-d '{"first_name": "Derek","last_name":"Pippin","whatsapp_number":"+17097715066","company_id":"099e597f-5ecc-48c8-a153-42b26a95a350","email":"kanaloa.dagim@usweek.net"}' \
https://kjl3fd1a21.execute-api.eu-central-1.amazonaws.com/dev/api/employee


curl -v -X PUT \
-H 'Content-Type: application/json' \
-d '{"first_name":"Russ","last_name":"Hannemon","whatsapp_number":"+8801711549547","email":"osbaldo.jathaniel@sixdrops.org"}' \
https://kjl3fd1a21.execute-api.eu-central-1.amazonaws.com/dev/admin/superAdmin



curl -v -X GET \
-H "X-Cognito-ID-Token:eyJraWQiOiJJd0V3M3N0N0NJQVlxb1ZhcEMrTm9hZTZmQjYzczVtYmdlTDlFQzQ1Y2ljPSIsImFsZyI6IlJTMjU2In0.eyJzdWIiOiIxM2IwNmZlMy00ZjI4LTQ1NWItOWQ4Zi00NDUyMjIxMGJlMzYiLCJpc3MiOiJodHRwczpcL1wvY29nbml0by1pZHAuZXUtY2VudHJhbC0xLmFtYXpvbmF3cy5jb21cL2V1LWNlbnRyYWwtMV90R1Y0V0pmUjYiLCJjb2duaXRvOnVzZXJuYW1lIjoiMTNiMDZmZTMtNGYyOC00NTViLTlkOGYtNDQ1MjIyMTBiZTM2IiwiZ2l2ZW5fbmFtZSI6IlJ1c3MiLCJjdXN0b206Y29tcGFueV9pZCI6Ik5cL0EiLCJhdWQiOiI0aDdocGxqYjkwdG1pNXNqMmM2ajU4azRjdCIsImV2ZW50X2lkIjoiOGJhNmM3NWItNDM3OS00NGFkLTgyZWItOGFmYmY1ODNkYjM2IiwidG9rZW5fdXNlIjoiaWQiLCJhdXRoX3RpbWUiOjE2MTE1ODI5MTcsInBob25lX251bWJlciI6Iis4ODAxNzExNTQ5NTQ3IiwiZXhwIjoxNjExNTg2NTE3LCJjdXN0b206cm9sZSI6ImFkbWluIiwiaWF0IjoxNjExNTgyOTE3LCJmYW1pbHlfbmFtZSI6Ikhhbm5lbW9uIiwiZW1haWwiOiJvc2JhbGRvLmphdGhhbmllbEBzaXhkcm9wcy5vcmcifQ.hTC2rTa70MW5dhVLkwfIat3ysnMbyiAj1JpvSv6oii8xHpGO4vwHaEQsXr9f49W57edvruQXcJeCyhnLf8i5yQYDdPz7lDVeY3D_Y6kFEdOQ-v35BK7Qp42XjZ2R67U2p8QP8412mPiVA9tTr09xbFXRoDkvVV3NoEtMcWPnwxJdiHy19ipYGqtz6Wk8ku-malRsJncUiHrO4Vx9yKol9xuXe9NGR2QqFkg1g9J-LLS-85SQMomuTc7RKwsfJfvsFB4BDdIZC0W3hXsz6Nv9LiOKorAMRyFDpZ_Rjl_xIuaGGplksXNKqrri11CYUwB0Z1J9Hsz0TtHK7kcFUSnOoA" \
-H "X-Cognito-Access-Token:eyJraWQiOiJteU5NZVRGWVQwZ2h6VjNiR3R3ZzVSeG9XaXNxb0JjUWFGdHU2eXQ2dWE0PSIsImFsZyI6IlJTMjU2In0.eyJzdWIiOiIxM2IwNmZlMy00ZjI4LTQ1NWItOWQ4Zi00NDUyMjIxMGJlMzYiLCJldmVudF9pZCI6IjhiYTZjNzViLTQzNzktNDRhZC04MmViLThhZmJmNTgzZGIzNiIsInRva2VuX3VzZSI6ImFjY2VzcyIsInNjb3BlIjoiYXdzLmNvZ25pdG8uc2lnbmluLnVzZXIuYWRtaW4iLCJhdXRoX3RpbWUiOjE2MTE1ODI5MTcsImlzcyI6Imh0dHBzOlwvXC9jb2duaXRvLWlkcC5ldS1jZW50cmFsLTEuYW1hem9uYXdzLmNvbVwvZXUtY2VudHJhbC0xX3RHVjRXSmZSNiIsImV4cCI6MTYxMTU4NjUxNywiaWF0IjoxNjExNTgyOTE3LCJqdGkiOiI5MTVhYmM0ZS0yMjIzLTQ4MDAtYjk1OS1hNTBkNWZiY2UxMDIiLCJjbGllbnRfaWQiOiI0aDdocGxqYjkwdG1pNXNqMmM2ajU4azRjdCIsInVzZXJuYW1lIjoiMTNiMDZmZTMtNGYyOC00NTViLTlkOGYtNDQ1MjIyMTBiZTM2In0.nZjtqg0J7X_pJlwZEBdK8DVdcN7gOjw-xFatxOkRsUvOrBXGA9AFdTSg5AKbioP0Mcnjy7AjLLfzSD_meEjVvbXqsjhRblDwMw6ryKY8sc_s_sltTCIwwaLvoNur-sUzLCtML3ceR_pSjhc3kuWbgYU6oMwuN_tVqVZ3fzp_e4UImJHb7Z1GssgVgL9cHJWNzqA2hUY5MeFmPLUHij_okddjbIofQsdx0yObJf6U9ysSS51jt5RsRekkGMtnF_njddCtcjadGrTSgnCP460CAgqxRkDncEgmO122_52IiTKkPuwZY8D1G2VQIkohMOIkwn9kMf0eqsoZa41TDh2zEw" \
https://kjl3fd1a21.execute-api.eu-central-1.amazonaws.com/dev/admin/agency/914f174c-12bb-44c2-acca-6af7c863304c






curl -v -X PUT \
-H 'Content-Type: multipart/form-data' \
-H 'X-Cognito-Access-Token:eyJraWQiOiJJd0V3M3N0N0NJQVlxb1ZhcEMrTm9hZTZmQjYzczVtYmdlTDlFQzQ1Y2ljPSIsImFsZyI6IlJTMjU2In0.eyJzdWIiOiIzNTc1NjE1NC1jNDc4LTQ1OGQtODBiZC1iYTNhZTI2YjIwZWMiLCJlbWFpbF92ZXJpZmllZCI6dHJ1ZSwiaXNzIjoiaHR0cHM6XC9cL2NvZ25pdG8taWRwLmV1LWNlbnRyYWwtMS5hbWF6b25hd3MuY29tXC9ldS1jZW50cmFsLTFfdEdWNFdKZlI2IiwiY29nbml0bzp1c2VybmFtZSI6IjM1NzU2MTU0LWM0NzgtNDU4ZC04MGJkLWJhM2FlMjZiMjBlYyIsImdpdmVuX25hbWUiOiJCZXJ0cmFtIiwiY3VzdG9tOmNvbXBhbnlfaWQiOiI0ZDI5NzhhMC03NjMwLTQyODQtOGZjNC01MzVkOTNjY2UyNWEiLCJhdWQiOiI0aDdocGxqYjkwdG1pNXNqMmM2ajU4azRjdCIsImV2ZW50X2lkIjoiYzM5MjBlOWEtZjZmZC00YjJjLTkxYTYtZDZhYzJmNDhhMWZhIiwidG9rZW5fdXNlIjoiaWQiLCJhdXRoX3RpbWUiOjE2MTIzOTI4NzEsInBob25lX251bWJlciI6Iis4ODAxNzExNTQ5NTQ4IiwiZXhwIjoxNjEyMzk2NDcxLCJjdXN0b206cm9sZSI6ImFnZW5jeV9hZG1pbiIsImlhdCI6MTYxMjM5Mjg3MiwiZmFtaWx5X25hbWUiOiJHaWxmb3lsZSIsImVtYWlsIjoiYWJyYXJjYWxjdWxhc0BnbWFpbC5jb20ifQ.b3TTMVTjdB4yOcS41LqHLFybCOFKX1k_lEuO-0XYG_hrxMlpHCGZ_9-NXF5MTaBD-aUxr6xSALmBwkBGu8XD-WGCgAApAGY_e2LYmpCoeUPCIVFgSMCW3cY8xvOYKSk210l5NwUuvpmWfkA33cgKakOcfi1LGTjqHu-LYlsXfUBfj0njUomuVxkgBw7SqzbgE6RQloF59hBRDXTC-TBlFM4K8kxHQ-XnoWsVJmKmA6vIGWVfc5BYydhw4JeKQcXTfdSYBTo9ZqX17DbSh6ePmMgSuYVM6LwNKTe_0alm-r-BcyTpIiFrND6OQCiHe1uUNZlwz2l7Sd4Z-GR7p1ipMQ' \
-H 'X-Cognito-ID-Token:eyJraWQiOiJteU5NZVRGWVQwZ2h6VjNiR3R3ZzVSeG9XaXNxb0JjUWFGdHU2eXQ2dWE0PSIsImFsZyI6IlJTMjU2In0.eyJzdWIiOiIzNTc1NjE1NC1jNDc4LTQ1OGQtODBiZC1iYTNhZTI2YjIwZWMiLCJldmVudF9pZCI6ImMzOTIwZTlhLWY2ZmQtNGIyYy05MWE2LWQ2YWMyZjQ4YTFmYSIsInRva2VuX3VzZSI6ImFjY2VzcyIsInNjb3BlIjoiYXdzLmNvZ25pdG8uc2lnbmluLnVzZXIuYWRtaW4iLCJhdXRoX3RpbWUiOjE2MTIzOTI4NzEsImlzcyI6Imh0dHBzOlwvXC9jb2duaXRvLWlkcC5ldS1jZW50cmFsLTEuYW1hem9uYXdzLmNvbVwvZXUtY2VudHJhbC0xX3RHVjRXSmZSNiIsImV4cCI6MTYxMjM5NjQ3MSwiaWF0IjoxNjEyMzkyODcyLCJqdGkiOiI2MmI0OWIzMi00ZTJkLTRiNDYtOGNhZi03NjZmYzI4OTRkOWEiLCJjbGllbnRfaWQiOiI0aDdocGxqYjkwdG1pNXNqMmM2ajU4azRjdCIsInVzZXJuYW1lIjoiMzU3NTYxNTQtYzQ3OC00NThkLTgwYmQtYmEzYWUyNmIyMGVjIn0.SnJ0hwPsJRv8vYy1GDOHbZgukFlTB2-wlDxfbaNVBdFpB7ysG1B_iDIW2iZXZPRVdapYWTANwy-sGotn1s0sFthD9rQwQrKYYSaeBAAFbfelJLtBLzTM8de16u61MYYj3e8cxKPAqS4wUP-vqp6PGo30Mb1FUFvpj38dGA4tALJ1C0QuAzsMxGCyxGYYLR7PfOXpiNSyjQxPeVeBHz2skPXzu6fIx_hezZum4stIBM36BK4i7DG9f8t24Iddf1kI-SvFBv5Ljdyb8nDMOUfHxvvddULvLROeY5Cz_biKXmt8Poi_2e-qHQLJLgcNaX7W4L8NzEND7m6TzvKa_aZQ6g' \
-F 'company_id=099e597f-5ecc-48c8-a153-42b26a95a350' \
-F 'keywords[]=Monthly' \
-F 'keywords[]=Productivity' \
-F 'name=MonthlyOutputs' \
-F 'task_number=1' \
-F 'query_id=05a3fea2-8eb5-42c3-b5e1-f19daa750025' \
-F 'frequency=1' \
-F 'file=@/home/zerocool/freelancer/serverless-api/crud.txt' \
https://6vaj70yx1j.execute-api.eu-central-1.amazonaws.com/dev/api/task

6vaj70yx1j


Broadstroke
aid: 1adf4dfd-f74d-4022-b1dc-c5a8ccc249b3

cid: b0402695-4cda-4b11-a5df-0881b4f2526d

aws cognito-idp admin-initiate-auth --user-pool-id eu-central-1_tGV4WJfR6 --client-id 4h7hpljb90tmi5sj2c6j58k4ct --auth-flow ADMIN_NO_SRP_AUTH --auth-parameters USERNAME=+8801711549548,PASSWORD=Sinkingone@456789


curl -v -X GET https://kjl3fd1a21.execute-api.eu-central-1.amazonaws.com/dev/api/company/099e597f-5ecc-48c8-a153-42b26a95a350