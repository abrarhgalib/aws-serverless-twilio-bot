'use strict';
const awsServerlessExpress = require('aws-serverless-express');
const { MessageService, ApiService, AdminService } = require('./src/index');


const MessageServer = awsServerlessExpress.createServer(MessageService);
const ApiServer = awsServerlessExpress.createServer(ApiService);
const AdminServer = awsServerlessExpress.createServer(AdminService);

exports.MessageHandler = (event, context) => {
  return awsServerlessExpress.proxy(MessageServer, event, context);
};

exports.ApiHandler = (event, context) => {
  return awsServerlessExpress.proxy(ApiServer, event, context);
};

exports.AdminHandler = (event, context) => {
  return awsServerlessExpress.proxy(AdminServer, event, context);
};

