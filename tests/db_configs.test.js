const { expect, assert } = require('chai');
const request = require('supertest');


const BASE_URL = 'https://ycwgd6l03i.execute-api.eu-central-1.amazonaws.com/dev';
const DEF_DELAY = 10000;


function delay(interval) {
   return it(`should be a delay of ${interval/1000} seconds`, done => {
      setTimeout(() => done(), interval)

   }).timeout(interval + 100) 
   // The extra 100ms should guarantee the test will not fail due to exceeded timeout
}

async function Null() {
    return new Promise(res => res(null));
}

const req = request(BASE_URL);

describe('Tests for /db_configs', function() {
    let company_id = 'd67b0f83-ecc6-4598-8410-931ac5df0c0c';
    let db_config_id;

    it('creates a new db_config', async () => {
        try {
            const new_db_config = {
                db_name: 'Ads Database',
                db_connection_url: 'https://12.67.121.67:3000?user=admin&password=adminPass&db=ads',
                company_id: company_id
            };
            const response = await req.put('/api/db_config')
                .send(new_db_config)
                .set('Content-Type', 'application/json')
                .expect(201);
            console.log(response.body.db_config);
            if(response.body.db_config.db_config_id) {
                console.log(`Created config with ID: ${response.body.db_config.db_config_id}`);
                db_config_id = response.body.db_config.db_config_id;
            }
            return Null();
        } catch(err) {
            throw err;
        }
    });
    delay(DEF_DELAY);

    it('gets an existing db_config', async () => {
        try {
            const response = await req.get(`/api/db_config/${db_config_id}`)
                .expect(200);
            console.log(response.body.db_config);
            return Null();
        } catch(err) {
            throw err;
        }
    });
    delay(DEF_DELAY);

    it('deletes an existing db_config', async () => {
        try {
            const response = await req.delete(`/api/db_config/${db_config_id}`)
                .expect(200);
            console.log(response.body);
            return Null();
        } catch(err) {
            throw err;
        }
    });
    delay(DEF_DELAY);

});