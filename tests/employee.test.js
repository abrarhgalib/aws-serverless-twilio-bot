const { expect, assert } = require('chai');
const request = require('supertest');


const BASE_URL = 'https://ycwgd6l03i.execute-api.eu-central-1.amazonaws.com/dev';
const DEF_DELAY = 10000;


function delay(interval) {
   return it(`should be a delay of ${interval/1000} seconds`, done => {
      setTimeout(() => done(), interval)

   }).timeout(interval + 100) 
   // The extra 100ms should guarantee the test will not fail due to exceeded timeout
}

async function Null() {
    return new Promise(res => res(null));
}

const req = request(BASE_URL);

describe('Tests for /employee', function() {
    let company_id = '68cef049-d722-46f8-8b52-48a247ed5998';
    let new_employee_id;

    it('creates a new employee', async () => {
        try {
            const new_employee = {
                first_name: 'Moss',
                last_name: 'Hendricks',
                company_id: company_id,
                whatsapp_number: '+8801847090622',
                email: 'abrarcalculas@gmail.com'
            };
            const response = await req.put('/api/employee')
                .send(new_employee)
                .set('Content-Type', 'application/json')
                .expect(201);
            console.log(response.body.employee);
            if(response.body.employee.employee_id) {
                new_employee_id = response.body.employee.employee_id;
                console.log(`Created employee with ID: ${new_employee_id}`);
            }
            return Null();
        } catch(err) {
            throw err;
        }
    });
    delay(DEF_DELAY);

    it('gets an employee', async () => {
        try {
            const response = await req.get(`/api/employee/${new_employee_id}`)
                .expect(200);
            console.log(response.body.employee);
            return Null();
        } catch(err) {
            throw err;
        }
    });
    delay(DEF_DELAY);

    it('deletes an employee', async () => {
        try {
            const response = await req.delete(`/api/employee/${new_employee_id}`)
                .expect(200);
            
            console.log(response.body);
            return Null();
        } catch(err) {
            throw err;
        }
    });
    delay(DEF_DELAY);
    
});