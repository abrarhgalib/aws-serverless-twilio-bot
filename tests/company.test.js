const request = require('supertest');
const { expect, should } = require('chai');

const BASE_URL = 'https://kjl3fd1a21.execute-api.eu-central-1.amazonaws.com/dev';
const DEF_DELAY = 10000;


function delay(interval) {
   return it(`should be a delay of ${interval/1000} seconds`, done => {
      setTimeout(() => done(), interval)

   }).timeout(interval + 100) 
   // The extra 100ms should guarantee the test will not fail due to exceeded timeout
}


const req = request(BASE_URL);

describe('Tests for /company', function() {
    let company_id = '099e597f-5ecc-48c8-a153-42b26a95a350';
    let agency_id = '4d2978a0-7630-4284-8fc4-535d93cce25a';

    
    it('creates a company', async () => {
        try {
            const new_company = {
                agency_id: agency_id,
                company_name: 'Broadcom Asia'
            };
            const response = await req.put('/api/company')
                .send(new_company)
                .set('Content-Type', 'application/json')
                .expect(201);
            expect(typeof response.body.company).to.eql('object');
            company_id = response.body.company.company_id;
            console.log(response.body.company);

            return null;
        } catch(err) {
            console.error(err);
            throw err;
        }
    });
    
    delay(DEF_DELAY);
    it('gets a company', async () => {
        try {
            console.log('Getting company: ' + company_id);
            const response = await req.get(`/api/company/${company_id}`)
                .expect(200);
            expect(typeof response.body.company).to.eql('object');
            console.log(response.body.company);

            return null;
        } catch(err) {
            console.error(err);
            throw err;
        }
    });
    /*
    it('gets all employees for a company', async () => {
        try {
            const response = await req.get(`/api/employees/${company_id}`)
                .expect(200);
            console.log(response.body.employees);

            return null;
        } catch(err) {
            throw err;
        }
    })
    /*
    it('gets all db_configs for a company', async () => {

    });
    */
    delay(DEF_DELAY);
    it('gets all companies', async () => {
        try {
            const response = await req.get('/api/companies')
                .expect(200);
            expect(typeof response.body.companies).to.eql('object');
            console.log(response.body.companies);

            return null;
        } catch(err) {
            console.error(err);
            throw err;
        }
    });
    delay(DEF_DELAY);
    it('gets all companies for an agency', async () => {
        try {
            const response = await req.get(`/api/companies/${agency_id}`)
                .expect(200);
            expect(typeof response.body.companies).to.eql('object');
            console.log(response.body.companies);
        
            return null;
        } catch(err) {
            console.error(err);
            throw err;
        }
    });
    /*it('gets all tasks for all companies', async () => {
        try {
            const response = await req.get('/api/companies/tasks')
                .expect(200);
            console.log(response.body.tasks);
            //company_id = response.body.tasks[0].company_id;
            return null;
        } catch(err) {
            console.error(err);
            throw err;
        }
    });*/

    /*delay(DEF_DELAY);
    it('gets all tasks for a company', async () => {
        try {
            const response = await req.get(`/api/company/${company_id}/tasks`)
                .expect(200);
            console.log(response.body.tasks);
            return null;
        } catch(err) {
            console.error(err);
            throw err;
        }
    });*/
});