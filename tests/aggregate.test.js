const { expect, assert, util } = require('chai');
const request = require('supertest');


const BASE_URL = 'https://ycwgd6l03i.execute-api.eu-central-1.amazonaws.com/dev';
const DEF_DELAY = 10000;


function delay(interval) {
   return it(`should be a delay of ${interval/1000} seconds`, done => {
      setTimeout(() => done(), interval)

   }).timeout(interval + 100) 
   // The extra 100ms should guarantee the test will not fail due to exceeded timeout
}

async function Null() {
    return new Promise(res => res(null));
}

const req = request(BASE_URL);

describe('Tests for all aggregate endpoints', function() {
    
    let agency_id = 'bfbbb667-a972-490d-aba4-c4ed561a4fb3';
    let company_id = 'd67b0f83-ecc6-4598-8410-931ac5df0c0c';
    
    it('gets all companies', async () => {
        try {
            const response = await req.get('/api/companies')
                .expect(200);
            console.log(response.body.companies);
            return Null();
        } catch(err) {
            throw err;
        }
    });
    delay(DEF_DELAY);

    it('gets all companies for an agency', async () => {
        try {
            const response = await req.get(`/api/companies/${agency_id}`)
                .expect(200);
            console.log(response.body.companies);
            return Null();
        } catch(err) {
            throw err;
        }
    });
    delay(DEF_DELAY);

    it('gets all employees', async () => {
        try {
            const response = await req.get('/api/employees')
                .expect(200);
            console.log(response.body.employees);
        } catch(err) {
            throw err;
        }
    });
    delay(DEF_DELAY);

    it('gets all employees for a company', async () => {
        try {
            const response = await req.get(`/api/employees/${company_id}`)
                .expect(200);
            console.log(response.body.employees);
            return Null();
        } catch(err) {
            throw err;
        }
    });
    delay(DEF_DELAY);

    it('get all queries', async () => {
        try {
            const response = await req.get('/api/queries')
                .expect(200);
            console.log(response.body.queries);
            return Null();
        } catch(err) {
            throw err;
        }
    });
    delay(DEF_DELAY);

    it('get all queries for a company', async () => {
        try {
            const response = await req.get(`/api/queries/${company_id}`)
                .expect(200);
            console.log(response.body.queries);
            return Null();
        } catch(err) {
            throw err;
        }
    });
    delay(DEF_DELAY);

    it('get all db_configs', async () => {
        try {
            const response = await req.get('/api/db_configs')
                .expect(200);
            console.log(response.body.db_configs);
            return Null();
        } catch(err) {
            throw err;
        }
    });
    delay(DEF_DELAY);

    it('get all db_configs for a company', async () => {
        try {
            const response = await req.get(`/api/db_configs/${company_id}`)
                .expect(200);
            console.log(response.body.db_configs);
            return Null();
        } catch(err) {
            throw err;
        }
    });
    delay(DEF_DELAY);

    it('get all tasks', async () => {
        try {
            const response = await req.get('/api/companies/tasks/all')
                .expect(200);
            console.log(response.body.tasks);
            return Null();
        } catch(err) {
            throw err;
        }
    });
    delay(DEF_DELAY);

    it('get all tasks for a company', async () => {
        try {
            const response = await req.get(`/api/company/${company_id}/tasks`)
                .expect(200);
            console.log(response.body.tasks);
            return Null();
        } catch(err) {
            throw err;
        }
    });
    
});