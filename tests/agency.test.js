const request = require('supertest');
const { expect, should } = require('chai');

const BASE_URL = 'https://kjl3fd1a21.execute-api.eu-central-1.amazonaws.com/dev';
const DEF_DELAY = 10000;


function delay(interval) {
   return it(`should be a delay of ${interval/1000} seconds`, done => {
      setTimeout(() => done(), interval)

   }).timeout(interval + 100) 
   // The extra 100ms should guarantee the test will not fail due to exceeded timeout
}


const req = request(BASE_URL);

describe('Tests for /agency', function() {
    // test variables
    let agency;
    let agency_id;

    
    it('create a new agency', async () => {
        const new_agency = {
            name: 'Square Enix',
            agency_admin_phone_number: '+17097776164',
            agency_admin_email: 'abrarhgalib@gmail.com',
        };
        try {
            const response = await req.put('/admin/agency')
                .send(new_agency)
                .set('Content-Type', 'application/json')
                .expect(201);
            expect(typeof response.body.agency_id).to.eql('string');
            agency_id = response.body.agency_id;
            return new Promise((res) => {
                console.log(`Agency-ID: ${agency_id}`);
                res();
            });
            //done();
        } catch(err) {
            //done(err);
            return new Promise((_, rej) => rej('Error in test'));
        }
    });
    delay(DEF_DELAY); 
    it('creates an agency admin user', async () => {
        const new_agency_admin = {
            first_name: 'Abrar',
            last_name: 'Galib',
            whatsapp_number: '+8801847090622',
            email: 'abrarhgalib@gmail.com',
        };
        try {
            const response = await req.put(`/admin/agency/${agency_id}/admin`)
                .send(new_agency_admin)
                .set('Content-Type', 'application/json')
                .expect(201);
            expect(typeof response.body).to.eql('object');
            return new Promise((res) => {
                console.log(response.body);
                res();
            });
            //done();
        } catch(err) {
            //done(err);
            return new Promise((_, rej) => rej('Error in test'));
        }
    });
    
    delay(DEF_DELAY); 
    it('get an existing agency', async () => {
        try {
            const response = await req.get(`/admin/agency/${agency_id}`)
                .expect(200);
            expect(typeof response.body).to.eql('object');
            expect(typeof response.body.agency).to.eql('object');
            return new Promise(res => {
                console.log(response.body);
                res()
            });
        } catch(err) {
            return new Promise((_, rej) => rej('Error'));
        }
    });

    /*
    it('deletes an existing agency', async () => {

    });*/
    delay(DEF_DELAY);
    it('gets all registered agencies', async () => {
        try {
            const response = await req.get('/admin/agencies')
                .expect(200);
            expect(typeof response.body).to.eql('object');
            expect(typeof response.body.agencies).to.eql('object');
            expect(typeof response.body.agencies.length).to.eql('number');

            return new Promise(res => {
                console.log(response.body.agencies);
                res();
            });
        } catch(err) {
            return new Promise((_, rej) => rej('Error'));
        }
    });
});