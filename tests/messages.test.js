const qs = require('querystring');
const request = require('supertest');
const { expect, should } = require('chai');

const BASE_URL = 'https://6vaj70yx1j.execute-api.eu-central-1.amazonaws.com/dev';
const DEF_DELAY = 10000;


function delay(interval) {
   return it(`should be a delay of ${interval/1000} seconds`, done => {
      setTimeout(() => done(), interval)

   }).timeout(interval + 100) 
   // The extra 100ms should guarantee the test will not fail due to exceeded timeout
}
async function Null() {
    return new Promise(res => res(null));
}


const req = request(`${BASE_URL}/message/incoming`);

describe('Tests for webhooks', function() {
    // %2B for + sign encoding
    const From = 'whatsapp:%2B8801847090622';
    const To = 'whatsapp:%2B17047616560';
    const SmsStatus = 'received';
    const ContentType = 'application/x-www-form-urlencoded';

    /*it('gets the help menu', async () => {
        try {
            const q = qs.stringify({ 
                From, To, SmsStatus,
                Body: 'H'
            });
            const response = await req.post('/')
                .send(`From=${From}`)
                .send(`To=${To}`)
                .send(`SmsStatus=${SmsStatus}`)
                .send(`Body=H`)
                .expect(200);
            
            console.log(response.text);
            
            return Null();
        } catch(err) {
            throw err;
        }
    });
    delay(DEF_DELAY);

    it('gets the system menu', async () => {
        try {
            const q = qs.stringify({ 
                From, To, SmsStatus,
                Body: 'S 999'
            });
            const response = await req.post('/')
                .send(`From=${From}`)
                .send(`To=${To}`)
                .send(`SmsStatus=${SmsStatus}`)
                .send(`Body=S 999`)
                .expect(200);
            
            console.log(response.text);
            
            return Null();
        } catch(err) {
            throw err;
        }
    });
    delay(DEF_DELAY);

    
    it('gets all employees', async () => {
        try {
            const q = qs.stringify({ 
                From, To, SmsStatus,
                Body: 'S 900'
            });
            const response = await req.post('/')
                .send(`From=${From}`)
                .send(`To=${To}`)
                .send(`SmsStatus=${SmsStatus}`)
                .send(`Body=S 900`)
                .expect(200);
            
            console.log(response.text);
            
            return Null();
        } catch(err) {
            throw err;
        }
    });
    delay(DEF_DELAY);

    it('add an employee', async () => {
        try {
            const q = qs.stringify({ 
                From, To, SmsStatus,
                Body: 'S 901'
            });
            const response = await req.post('/')
                .send(`From=${From}`)
                .send(`To=${To}`)
                .send(`SmsStatus=${SmsStatus}`)
                .send(`Body=S 901`)
                .expect(200);
            
            console.log(response.text);
            
            return Null();
        } catch(err) {
            throw err;
        }
    });
    delay(DEF_DELAY);

    it('delete an employee', async () => {
        try {
            const q = qs.stringify({ 
                From, To, SmsStatus,
                Body: 'S 902'
            });
            const response = await req.post('/')
                .send(`From=${From}`)
                .send(`To=${To}`)
                .send(`SmsStatus=${SmsStatus}`)
                .send(`Body=S 902`)
                .expect(200);
            
            console.log(response.text);
            
            return Null();
        } catch(err) {
            throw err;
        }
    });
    delay(DEF_DELAY);

    it('gets all companies', async () => {
        try {
            const q = qs.stringify({ 
                From, To, SmsStatus,
                Body: 'S 999'
            });
            const response = await req.post('/')
                .send(`From=${From}`)
                .send(`To=${To}`)
                .send(`SmsStatus=${SmsStatus}`)
                .send(`Body=S 903`)
                .expect(200);
            
            console.log(response.text);
            
            return Null();
        } catch(err) {
            throw err;
        }
    });
    delay(DEF_DELAY);

    it('add a company', async () => {
        try {
            const q = qs.stringify({ 
                From, To, SmsStatus,
                Body: 'S 904'
            });
            const response = await req.post('/')
                .send(`From=${From}`)
                .send(`To=${To}`)
                .send(`SmsStatus=${SmsStatus}`)
                .send(`Body=S 904`)
                .expect(200);
            
            console.log(response.text);
            
            return Null();
        } catch(err) {
            throw err;
        }
    });
    delay(DEF_DELAY);

    it('delete a company', async () => {
        try {
            const q = qs.stringify({ 
                From, To, SmsStatus,
                Body: 'S 905'
            });
            const response = await req.post('/')
                .send(`From=${From}`)
                .send(`To=${To}`)
                .send(`SmsStatus=${SmsStatus}`)
                .send(`Body=S 905`)
                .expect(200);
            
            console.log(response.text);
            
            return Null();
        } catch(err) {
            throw err;
        }
    });
    delay(DEF_DELAY);

    it('app download link', async () => {
        try {
            const q = qs.stringify({ 
                From, To, SmsStatus,
                Body: 'S 906'
            });
            const response = await req.post('/')
                .send(`From=${From}`)
                .send(`To=${To}`)
                .send(`SmsStatus=${SmsStatus}`)
                .send(`Body=S 906`)
                .expect(200);
            
            console.log(response.text);
            
            return Null();
        } catch(err) {
            throw err;
        }
    });
    delay(DEF_DELAY);

    it('manual download link', async () => {
        try {
            const q = qs.stringify({ 
                From, To, SmsStatus,
                Body: 'S 907'
            });
            const response = await req.post('/')
                .send(`From=${From}`)
                .send(`To=${To}`)
                .send(`SmsStatus=${SmsStatus}`)
                .send(`Body=S 907`)
                .expect(200);
            
            console.log(response.text);
            
            return Null();
        } catch(err) {
            throw err;
        }
    });
    delay(DEF_DELAY);

    it('set default company', async () => {
        try {
            const q = qs.stringify({ 
                From, To, SmsStatus,
                Body: 'S 908'
            });
            const response = await req.post('/')
                .send(`From=${From}`)
                .send(`To=${To}`)
                .send(`SmsStatus=${SmsStatus}`)
                .send(`Body=S 908`)
                .expect(200);
            
            console.log(response.text);
            
            return Null();
        } catch(err) {
            throw err;
        }
    });
    delay(DEF_DELAY); 

    it('gets task by keywords', async () => {
        try {
            const q = qs.stringify({ 
                From, To, SmsStatus,
                Body: 'S 908'
            });
            const response = await req.post('/')
                .send(`From=${From}`)
                .send(`To=${To}`)
                .send(`SmsStatus=${SmsStatus}`)
                .send(`Body=T Daily Ads`)
                .expect(200);
            
            console.log(response.text);
            
            return Null();
        } catch(err) {
            throw err;
        }
    });*/

    it('gets task by LT message', async (done) => {
        try {
            const q = qs.stringify({
                From, To, SmsStatus,
                Body: 'LT BCOMMED Ads'
            });
            const response= await req.post('/')
                .send(`From=${From}`)
                .send(`To=${To}`)
                .send(`SmsStatus=${SmsStatus}`)
                .send(`Body=LT BCOMMED Ads`)
                .expect(200);
            
            return new Promise(res => {
                console.log(response.text);
                res();
            });
        } catch(e) {
            throw e;
        }
    })
});
