/*
    * Admin API
    * ---------
    * Used for  creating agency users
*/
const { Agency } = require('./../models/Agency');
const { createAgencyAdmin, createSuperAdmin, resetAgencyAdminPassword } = require('./../models/AgencyAdmin');
const uuid = require('uuid');

// Helpers
function getAgencyId(req) {
    const { agency_id } = req.params;
    if(!agency_id) {
        return null;
    }
    return agency_id;
}
function getAgency(req) {
    const { name, agency_admin_email, agency_admin_phone_number, companies_quota_limit, employees_quota_limit } = req.body;
    if(!name || !agency_admin_email || !agency_admin_phone_number) {
        return null;
    }
    const agency_id = uuid.v4();
    return {
        agency_id, name, agency_admin_email, agency_admin_phone_number,
        companies_added: 0,
        employees_added: 0,
        companies_quota_limit: companies_quota_limit ? companies_quota_limit : 3,
        employees_quota_limit: employees_quota_limit ? employees_quota_limit : 5   
    };
}
function getAdminUser(req, superAdmin = false) {
    const { first_name, last_name, whatsapp_number, email } = req.body;
    if(!first_name || !last_name || !whatsapp_number || !email) {
        return null;
    }
    console.log(req.params);
    let { agency_id } = req.params;
    if(!superAdmin && !agency_id) {
        return null;
    } 
    if(agency_id) {
        //    
    } else {
        agency_id = 'N/A';
    }
    return {
        agency_id, first_name, last_name,
        whatsapp_number, email, 
    };
}

module.exports = function({ express, Aws }) {
    const router = express.Router({
        mergeParams: true
    });

    // BASIC crud
    router.put('/superAdmin', async (req, res) => {
        const admin = getAdminUser(req, true);
        if(!admin) {
            return res.status(400).send('Invalid Request');
        }
        try {
            const user = await createSuperAdmin({ ...admin, Aws });
            if(!user) {
                return res.status(500).send('Could not create admin user');
            }
            return res.status(201).json({ ...user });
        } catch(err) {
            console.error(err);
            return res.status(500).send('Internal Error');
        }
    });
    router.put('/agency/:agency_id/admin', async (req, res) => {
        // create an admin user for an existing agency
        const admin = getAdminUser(req);
        if(!admin) {
            return res.status(400).send('Invalid Request');
        }
        try {
            const user = await createAgencyAdmin({ ...admin, Aws });
            if(!user) {
                return res.status(500).send('Could not create admin user');
            }
            return res.status(201).json({ ...user });
        } catch(err) {
            console.error(err);
            return res.status(500).send('Internal Error');
        }
    });
    router.put('/agency', async (req, res) => {
        const agency = getAgency(req);
        try {
            const agencyDoc = await Agency.create(agency);
            if(!agencyDoc) {
                return res.status(500).send('Could not create agency');
            }
            return res.status(201).json({ agency_id: agency.agency_id });
        } catch(err) {
            console.error(err);
            return res.status(500).send('Internal Error');
        }
    });
    router.delete('/agency/:agency_id', async (req, res) => {
         const agency_id = getAgencyId(req);
         if(!agency_id) {
             return res.status(400).send('Invalid Request');
         }
         try {
            await Agency.delete(agency_id);

            return res.status(200).send('Agency Deleted');
         } catch(err) {
             console.error(err);
             return res.status(500).send('Internal Error');
         }
    });
    router.post('/agency/:agency_id', async (req, res) => {
        // update an agency details
    });
    router.get('/agency/:agency_id', async (req, res) => {
        const agency_id = req.params.agency_id;
        console.log(`Agency_ID: ${agency_id}`);
        if(!agency_id) {
            return res.status(400).send('Invalid Request');
        }
        try {
            const agency = await Agency.get(agency_id);
            if(!agency) {
                return res.status(404).send('No such agency found');
            }
            return res.status(200).json({
                agency: agency.toJSON()
            });
        } catch(err) {
            console.error(err);
            return res.status(500).send('Internal Error');
        }
    });
    router.get('/agencies', async (req, res) => {
        try {
            const agencies = await Agency.scanAll();
            
            return res.status(200).json({ agencies: agencies.map(m => m.toJSON()) });
        } catch(err) {
            console.error(err);
            return res.status(500).send('Internal Error');
        }
    });

    return router;
};