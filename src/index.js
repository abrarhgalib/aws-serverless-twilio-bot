const express = require('express');
const bodyParser = require('body-parser');
const awsServerlessExpressMiddleware = require('aws-serverless-express/middleware');
const cors = require('cors');
const aws = require('aws-sdk');

const docClient = new aws.DynamoDB.DocumentClient({apiVersion: '2012-08-10'});
const s3 = new aws.S3({
    accessKeyId: 'AKIATOLPQDQZFQBY3F5Y',
    secretAccessKey: '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',
    s3BucketEndpoint: false
});
const sqs = new aws.SQS({
    apiVersion: '2012-11-05'
});
const taskNotifier = require('./sqs')({ Sqs: sqs }).notify;

const Api = express();
const Message = express();
const Admin = express();

const MessageService = require('./messages/routes')({ 
    S3: s3,
    express,
    Aws: aws,
    Sqs: sqs
});
const ApiService = require('./api/routes')({
    S3: s3,
    notifier: taskNotifier,
    express,
    Aws: aws
});
const AdminApiService = require('./admin-api/routes')({
    express,
    Aws: aws
});


Api.use(bodyParser.json());
Api.use(bodyParser.urlencoded({ extended: true }));
Message.use(bodyParser.json());
Message.use(bodyParser.urlencoded({ extended: true }));
Admin.use(bodyParser.json());
Admin.use(bodyParser.urlencoded({ extended: true }));

Message.use(cors());
Api.use(cors());
Admin.use(cors());

// Expose context and event
Api.use(awsServerlessExpressMiddleware.eventContext());

Message.use('/message', MessageService);
Api.use('/api', ApiService);
Admin.use('/admin', AdminApiService);


module.exports.ApiService = Api;
module.exports.AdminService = Admin;
module.exports.MessageService = Message;