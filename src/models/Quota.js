const dynamoOrm = require('dynamoose');
const { scanAll } = require('./common');

const Quota = dynamoOrm.model('quotas', {
    'publish_date': {
        type: String,
        hashKey: true
    },
    'companies_per_agency': {
        type: Number
    },
    'employees_per_agency': {
        type: Number
    },
    'employees_per_company': {
        type: Number
    },
    'quota_number': {
        type: Number
    }
}, {
    create: false
});

module.exports.Quota = Quota;