const dynamoOrm = require('dynamoose');
const { scanAll } = require('./common');

const Request = dynamoOrm.model('user_requests', {
    'request_id': {
        type: String,
        hashKey: true
    },
    'user_whatsapp_number': String,
    'user_id': {
        type: String,
        index: {
            global: true,
            name: 'user_id-index'
        }
    },
    'creation_date_time': String,
    'task_id': {
        type: String,
        index: {
            global: true,
            name: 'task_id-index'
        }
    },
    'keywords_used': {
        type: Array,
        schema: [ String ]
    },
    'has_match': Boolean,
    'is_live': Boolean,
    'is_completed': Boolean,
    'completed_date_time': String
}, {
    create: false
});

module.exports.Request = Request;