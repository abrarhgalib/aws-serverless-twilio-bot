const dynamoOrm = require('dynamoose');
const { scanAll } = require('./common');

const TaskRequest = dynamoOrm.model('task_request', {
    'task_request_id': {
        type: String,
        hashKey: true
    },
    'user_whatsapp_number': String,
    'company_id': {
        type: String,
        index: {
            global: true,
            name: 'task_request_company_id-index'
        }
    },
    'agency_id': {
        type: String,
        index: {
            global: true,
            name: 'task_request_agency_id-index'
        }
    },
    'creation_date_time': String,
    'keywords_used': {
        type: Array,
        schema: [ String ]
    },
    'is_live': Boolean,
    'is_completed': Boolean,
    'completed_date_time': String
}, {
    create: false
});

module.exports.TaskRequest = TaskRequest;