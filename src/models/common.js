/*
    * Model Helper methods for
    * sharing among all defined models
*/
const passwordGenerator = require('generate-password');

module.exports = {
    scanAll: async function() {
        let results = await this.scan().exec();
        let lastKey = results.lastKey;
        while(lastKey) {
            const newResults = await this.scan().startAt(lastKey).exec();
            results = [ ...results, ...newResults ];
            lastKey = newResults.lastKey;
        }
        return results;
    },
    createCognitoUser: function({ 
        whatsapp_number, first_name, last_name,
        email, company_id, agency_id, role
    }) {
        return {
            UserPoolId: 'eu-central-1_tGV4WJfR6',
            Username: whatsapp_number,
            TemporaryPassword: passwordGenerator.generate({
                numbers: true,
                uppercase: true,
                lowercase: true,
                symbols: true,
                strict: true,
                length: 12
            }),
            DesiredDeliveryMediums: [
                'EMAIL'
            ],
            ClientMetadata: {
                // used for pre-signup trigger
            },
            UserAttributes: [
                {
                    Name: 'given_name',
                    Value: first_name
                },
                {
                    Name: 'family_name',
                    Value: last_name
                },
                {
                    Name: 'email',
                    Value: email
                },
                {
                    Name: 'phone_number',
                    Value: whatsapp_number
                },
                {
                    Name: 'custom:role',
                    Value: role
                },
                {
                    Name: 'custom:company_id',
                    Value: agency_id ? agency_id : company_id
                }
            ]
        };
    },
    UserCreatedStatuses: () => {
        return {
            FORCE_CHANGE_PASSWORD: 'FORCE_CHANGE_PASSWORD',
            RESET_REQUIRED: 'RESET_REQUIRED',
            UNCONFIRMED: 'UNCONFIRMED',
            CONFIRMED: 'CONFIRMED' 
        };
        
    }
};