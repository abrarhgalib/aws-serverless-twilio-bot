const dynamoOrm = require('dynamoose');
const { scanAll } = require('./common');

const Log = dynamoOrm.model('logs', {
    'log_id': {
        type: String,
        hashKey: true
    },
    'user_whatsapp_number': {
        type: String,
        index: {
            global: true,
            name: 'user_whatsapp_number-index'
        }
    },
    'creation_date_time': String,
    'request_endpoint': String,
    'user_role': String,
    'entity_id': String
}, {
    create: false
});

module.exports.Log = Log;