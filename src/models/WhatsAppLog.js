const dynamoOrm = require('dynamoose');
const { scanAll } = require('./common');

const WhatsAppLog = dynamoOrm.model('whats_app_logs', {
    'log_id': {
        type: String,
        hashKey: true
    },
    'user_whatsapp_number': {
        type: String,
        index: {
            global: true,
            name: 'user_whatsapp_number-index'
        }
    },
    'creation_date_time': String,
    'message': String
}, {
    create: false
});

module.exports.WhatsAppLog = WhatsAppLog;