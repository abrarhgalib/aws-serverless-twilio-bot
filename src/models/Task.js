const dynamoOrm = require('dynamoose');
const { scanAll } = require('./common');

const Task = dynamoOrm.model('tasks', {
    'task_id': {
        type: String,
        hashKey: true
    },
    'company_id': {
        type: String,
        'index': {
            global: true,
            name: 'company_id-index' 
        }
    },
    'task_request_id': {
        type: String,
        index: {
            global: true,
            name: 'task_request_id-index'
        }
    },
    'file_name': String,
    'file_mime_type': String,
    'query_id': String,
    'frequency': Number,
    'name': String,
    'task_number': Number,
    'keywords': {
        type: Array,
        schema: [ String ]
    },
    'last_status': {
        type: String
    }
}, {
    create: false
});

Task.methods.set('scanAll', scanAll);

module.exports.Task = Task;