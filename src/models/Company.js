const dynamoOrm = require('dynamoose');
const { scanAll } = require('./common');

const Company = dynamoOrm.model('companies', {
    'company_id': {
        type: String,
        hashKey: true
    },
    'company_name': String,
    'employees_added': {
        'type': Number,
        'default': 0
    },
    'agency_id': {
        type: String,
        index: {
            global: true,
            name: 'agency_id-index'
        }
    },
    'short_code': {
        type: String,
        index: {
            global: true,
            name: 'short_code-index'
        }
    },
    'tax_id': {
        type: String
    },
    'billing_address': {
        type: String
    },
    'mailing_address': {
        type: String
    }
}, {
    create: false
});

Company.methods.set('scanAll', scanAll);

module.exports.Company = Company;