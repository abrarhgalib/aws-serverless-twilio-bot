const randomString = require('crypto-random-string');

const { createCognitoUser, UserCreatedStatuses } = require('./common');

async function updateEmailVerified({ whatsapp_number, CognitoIdService }) {
    // needed to enable password recovery by email
    const p = {
        UserAttributes: [
            { Name: 'email_verified', Value: 'true' }
        ],
        UserPoolId: 'eu-central-1_tGV4WJfR6',
        Username: whatsapp_number
    };
    const email_verified = await (new Promise(res => {
        CognitoIdService.adminUpdateUserAttributes(p, (err, data) => {
            if(err) {
                console.error(err);
                res(null);
            } else {
                res(true);
            }
        });
    }));
    return email_verified;
}

const EUserStatus = UserCreatedStatuses();
module.exports.createAgencyAdmin = async function({ 
    whatsapp_number, first_name, last_name,
    email, agency_id,
    Aws
}) {
    const CognitoIdService = new Aws.CognitoIdentityServiceProvider();
    try {
        const user = await CognitoIdService.adminCreateUser(createCognitoUser({
            whatsapp_number, first_name, last_name,
            email, 
            role: 'agency_admin',
            company_id: agency_id

        })).promise();
        if(!user && !user.User) {
            return null;
        } 
        console.log(user);
        const status = user.User.UserStatus;
        switch(status) {
            case EUserStatus.CONFIRMED:
            case EUserStatus.FORCE_CHANGE_PASSWORD:
            case EUserStatus.UNCONFIRMED:
            case EUserStatus.RESET_REQUIRED:
                await updateEmailVerified({ whatsapp_number, CognitoIdService })
                return { username: user.User.Username, attributes: user.User.Attributes };
                // force verify email
            default:
                return null;
        }
    } catch(err) {
        console.error(err);
        return null;
    }
};
module.exports.resetAgencyAdminPassword = async function({
    whatsapp_number, Aws
}) {
    const CognitoIdService = new Aws.CognitoIdentityServiceProvider();
    try {
        const reset = await (new Promise(res => {
            const p = {
                UserPoolId: 'eu-central-1_tGV4WJfR6',
                Username: whatsapp_number
            };
            CognitoIdService.adminResetUserPassword(p, (err, data) => {
                if(err) {
                    console.error(err);
                    res(null);
                } else {
                    res(data);
                }
            });
        }));
        if(!reset) {
            return null;
        } else {
            return true;
        }
    } catch(err) {
        console.error(err);
        return null;
    }
}

module.exports.createSuperAdmin = async function({ 
    whatsapp_number, first_name, last_name,
    email, agency_id,
    Aws
}) {
    const CognitoIdService = new Aws.CognitoIdentityServiceProvider();
    try {
        const user = await CognitoIdService.adminCreateUser(createCognitoUser({
            whatsapp_number, first_name, last_name,
            email, 
            company_id: agency_id, // N/A 
            role: 'admin'
        })).promise();
        if(!user && !user.User) {
            return null;
        } 
        console.log(user);
        const status = user.User.UserStatus;
        switch(status) {
            case EUserStatus.CONFIRMED:
            case EUserStatus.FORCE_CHANGE_PASSWORD:
            case EUserStatus.UNCONFIRMED:
            case EUserStatus.RESET_REQUIRED:
                await updateEmailVerified({ whatsapp_number, CognitoIdService });
                return { username: user.User.Username, attributes: user.User.Attributes };
            default:
                return null;
        }
    } catch(err) {
        console.error(err);
        return null;
    }
};

