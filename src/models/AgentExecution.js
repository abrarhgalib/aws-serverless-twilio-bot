const dynamoOrm = require('dynamoose');
const { scanAll } = require('./common');

const EAgencyExecutionLogTypes = {
    CREATED: 'created',
    ACCESSED: 'accessed',
    MODIFIED: 'modified',
    DELETED: 'deleted'
};

const AgencyExecution = dynamoOrm.model('agency_executions', {
    'company_id': String,
    'execution_id': String,
    'agent_id': String,
    'creation_date_time': String,
    'log_type': [ 
        EAgencyExecutionLogTypes.ACCESSED,
        EAgencyExecutionLogTypes.CREATED,
        EAgencyExecutionLogTypes.DELETED,
        EAgencyExecutionLogTypes.MODIFIED
    ],
    'log_details': String
}, {
    create: false
});

module.exports.AgencyExecution = AgencyExecution;
module.exports.EAgencyExecutionLogTypes = EAgencyExecutionLogTypes;