const dynamoOrm = require('dynamoose');
const { scanAll } = require('./common');

const Query = dynamoOrm.model('queries', {
    'company_id': {
        type: String,
        index: {
            global: true,
            name: 'company_id-index'
        }
    },
    'db_config_id': {
        type: String,
        index:{
            global: true,
            name: 'db_config_id-index'
        }
    },
    'query_name': String,
    'query_string': String,
    'query_id': {
        type: String,
        hashKey: true
    },
    'last_status': {
        type: String
    }
}, {
    create: false
});

Query.methods.set('scanAll', scanAll);

module.exports.Query = Query;