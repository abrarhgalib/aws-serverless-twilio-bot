const dynamoOrm = require('dynamoose');
const { scanAll } = require('./common');

const DbConfig = dynamoOrm.model('db_configs', {
    'db_config_id': {
        type: String,
        hashKey: true
    },
    'company_id': {
        type: String,
        index: {
            global: true,
            name: 'company_id-index'
        }
    },
    'db_name': String,
    'db_connection_url': String,
    'db_connection_string': String,
    'database_type': String,
    'last_status': String
}, {
    create: false
});

DbConfig.methods.set('scanAll', scanAll);

module.exports.DbConfig = DbConfig;