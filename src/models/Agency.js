const dynamoOrm = require('dynamoose');
const { scanAll } = require('./common');

const Agency = dynamoOrm.model('agencies',{
    'agency_id': {
        type: String,
        hashKey: true
    },
    'name': String,
    'companies_added': Number,
    'employees_added': Number,
    'agency_admin_email': String, // ? match with cognito?
    'agency_admin_phone_number': {
        type: String,
        index: {
            global: true,
            name: 'agency_admin_phone_number-index'
        }
    },
    'companies_quota_limit': Number,
    'employees_quota_limit': Number
}, {
    create: false
});
// Helpers
Agency.methods.set('scanAll', scanAll);


module.exports.Agency =Agency;