const dynamoOrm = require('dynamoose');
const { scanAll, createCognitoUser, UserCreatedStatuses } = require('./common');

const EUserStatus = UserCreatedStatuses();

const Employee = dynamoOrm.model('employees', {
    // Employee Schema
    'employee_id': {
        type: String,
        hashKey: true
    },
    'first_name': String,
    'last_name': String,
    'company_id': {
        type: String,
        index: [
            {
                global: true,
                name: 'company_id-index'
            }
        ]
    },
    'monthly_quota': Number,
    'whatsapp_number': {
        type: String,
        index: [
            {
                global: true,
                name: 'number_index'
            }
        ]
    }
}, {
    create: false
});

Employee.methods.set('scanAll', scanAll);

module.exports.Employee = Employee;
module.exports.createAnEmployee = async function({
    first_name, last_name, 
    company_id, whatsapp_number, email,
    Aws
}) {
    const CognitoIdService = new Aws.CognitoIdentityServiceProvider();
    try {
        const user = await CognitoIdService.adminCreateUser(createCognitoUser({
            first_name, last_name, company_id,
            whatsapp_number, email, 
            role: 'employee', 
        })).promise();
        if(!user && !user.User) {
            return null;
        } 
        console.log(user);
        const status = user.User.UserStatus;
        switch(status) {
            case EUserStatus.CONFIRMED:
            case EUserStatus.FORCE_CHANGE_PASSWORD:
            case EUserStatus.UNCONFIRMED:
            case EUserStatus.RESET_REQUIRED:
                return { username: user.User.Username, attributes: user.User.Attributes };
            default:
                return null;
        }
    } catch(err) {
        console.error(err);
        return null;
    }
}