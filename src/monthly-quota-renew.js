const { Task } = require('./models/Task');

exports.renewMonthlyQuota = async (event, context, callback) => {    
    try {
        const tasks = await Task.scanAll();
        if(!tasks || tasks.length < 1) {
            return false;
        }
        await Promise.all(
            tasks.map(async task => {
                const json = task.toJSON();
                const updated = await Task.update({ task_id: json.task_id }, { monthly_quota: 200 });
                if(!updated) {
                    return false;
                }
                return true;
            })
        )
        .then(updates => {
            console.log(`Successfully updated ${updates.filter(u => u === true).length}/${tasks.length}`);                    
            callback(null, true);
        });
    } catch(e) {
        console.error(e);
        callback(e, null);
    }
}