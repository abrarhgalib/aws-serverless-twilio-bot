module.exports = function({ Sqs }) {
    const queueUrl = 'https://sqs.eu-central-1.amazonaws.com/236993911858/tasks-queue.fifo';
    return {
        notify: async ({ task_id, company_id, is_live, update_time }) => {
            const sqsTask = {
                MessageAttributes: {
                    'task_id': {
                        DataType: 'String',
                        StringValue: task_id
                    },
                    'company_id': {
                        DataType: 'String',
                        StringValue: company_id
                    },
                },
                MessageBody: JSON.stringify({ 
                    task_id, company_id, is_live, update_time 
                }),
                MessageDeduplicationId: `${task_id}::${update_time}`,
                MessageGroupId: `tasks-${company_id}`,
                QueueUrl: queueUrl
            };
            return Sqs.sendMessage(sqsTask).promise();
        },
        queue: async({ task_request_id, agency_id, company_id, update_time, is_live }) => {
            const sqsTask = {
                MessageAttributes: {
                    'task_request_id': {
                        DataType: 'String',
                        StringValue: task_request_id
                    },
                    'agency_id': {
                        DataType: 'String',
                        StringValue: agency_id
                    },
                },
                MessageBody: JSON.stringify({ 
                    task_request_id, agency_id, company_id, update_time, is_live
                }),
                MessageDeduplicationId: `${task_request_id}::${update_time}`,
                MessageGroupId: `tasks-${agency_id}`,
                QueueUrl: queueUrl
            };
            return Sqs.sendMessage(sqsTask).promise();
        }
    };
    return ;
};