/**
 * TwiML message response templates
 * --------------------------------
 */
module.exports = {
    systemMenu: () => {
        return `Bot System Menu\n
---------------\n
999 ---- Returns system menu\n
900 ---- Get all employees\n
901 ---- Add new employee\n
902 ---- Delete an employee\n
903 ---- Get all companies\n
904 ---- Add new company\n
905 ---- Delete a company\n
906 ---- Get app download link\n
907 ---- App manual download link\n
908 ---- Set default company\n
        `;
    },
    deleteUser: () => {
        return `To Delete a User, send the following:\n
Delete: <EMPLOYEE_NUMBER>\n
where, EMPLOYEE_NUMBER is the number of the employee\n
`;
    },
    helpMenu: () => {
        return `Bot Help Menu\n
---------------------\n
H   --- For getting this menu\n
S 90*   --- For service menu options\n
L Com* [ LN to LM | ...keywords ]   --- For listing tasks by company\n
T [ N | ...keywords ]   --- For getting tasks\n
`;
    },
    createEmployeeFillerTemplate: () => {
        return `<Create:employee> Create an employee\n
--------------------------------------------\n
* Replace <*> with actual value\n
first_name: <FIRST_NAME>\n
last_name: <LAST_NAME>\n
email: <EMAIL>\n
company_id: <COMPANY_ID>\n
whatsapp_number: <WHATSAPP_NUMBER>\n
`;
    },
    deletedTemplate: (entity) => {
        return `[\u2714] Deleted\n
-----------------\n
Deleted ${entity}\n
Request Successful\n
`;
    },
    createErrorTemplate: (entity) => {
        return `[x] Error\n
-----------------\n
Could not create ${entity}\n
Server Error\n
`;
    },
    createQuotaErrorTemplate: (entity) => {
        return `[x] Error\n
-------------------------\n
Could not create ${entity}\n
Quota Limit Reached\n
`;
    },
    deleteErrorTemplate: (entity) => {
        return `[x] Error\n
-----------------\n
Could not delete ${entity}\n
Server Error\n
`;
    },
    deleteEmployeeFillerTemplate: () => {
        return `<Delete:employee> Deletes an employee\n
-------------------------------------------\n
* Replace <*> with actual value\n
\n
employee_id: <COMPANY_ID>\n
`;
    },
    deleteCompanyFillerTemplate: () => {
        return `<Delete:company> Delete a company\n
-------------------------------------------\n
* Replace <*> with actual value\n
company_id: <COMPANY_ID>\n
`;
    },
    createCompanyFillerTemplate: () => {
        return `<Create:company> Create a company\n
----------------------------------------\n
* Replace <*> with actual value\n
company_name: <COMPANY_NAME>\n
short_code: <SHORT_CODE>\n
agency_id: <AGENCY_ID>\n
`;
    },
    setDefaultCompany: () => {
        return `<Create:company default> Set default company\n
----------------------------------------\n
* Replace <COMPANY_ID> with actual value\n
company_id: <COMPANY_ID>\n
`;
    },
    appDownloadLink: (link = 'https://abararhgalib.com') => {
        return `App Download link\n
---------------------------\n
To download the app, go ${link}\n
`;
    },
    manualDownloadLink: (link = 'https://abararhgalib.com') => {
        return `Manual Download link\n
------------------------------
To download the manual, go ${link}\n
`;
    },
    getTasksTemplate: (tasks, xmlResponse) => {
        tasks.forEach(t => {
            const msg = xmlResponse.message();

            msg.body(`Task\n
--------------\n
name: ${t.name}\n
keywords: ${t.keywords.join(', ').trim()}\n
number: ${t.task_number}\n
========================\n\n
`
            );
            msg.media(t.url);
        });
    },
    getCompaniesTemplate: (companies) => {
        return companies.map(c => {
            return `Company\n
-----------------\n
${Object.keys(c).sort().map(k => `${k}: ${c[k]}`).join('\n')}
===========================
`;
        }).join('\n');
    },
    getEmployeesTemplate: (employees) => {
        return employees.map(e => {
            return `Employee\n
----------------\n
${Object.keys(e).sort().map(k => `${k}: ${e[k]}`).join('\n')}
===================
`;
        }).join('\n');
    },
    taskLimitReachedTemplate: () => {
        const date = new Date();
        date.setDate(1);
        date.setMonth(new Date().getMonth() + 1);
        date.setFullYear(new Date().getFullYear());
        return `Monthly Quota Reached\n
---------------------------------------\n
Monthly quota exceeded for task assignment.\n
Quota renews on ${date.toISOString()}\n
`;
    },
    queuedTaskTemplate: (req) => {
        return `Task Queued
--------------------
Request ID: ${req.task_request_id},
Created At: ${req.creation_date_time}
`;
    }
};