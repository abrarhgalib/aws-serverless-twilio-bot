const twilio = require('twilio');
const uuid = require('uuid');

const MessageResponse = twilio.twiml.MessagingResponse;
const templates = require('./templates');

const { Company } = require('./../models/Company');
const { Employee, createAnEmployee } = require('./../models/Employee');
const { Task } = require('./../models/Task');
const { Agency } = require('./../models/Agency');
const { Request } = require('./../models/Request');
const { Quota } = require('./../models/Quota');
const { WhatsAppLog } = require('./../models/WhatsAppLog');
const { resetAgencyAdminPassword } = require('./../models/AgencyAdmin');
const { TaskRequest } = require('./../models/TaskRequest');

async function checkUserRole(whatsapp_number) {
    // for system messages only
    try {
        console.log(`Checking user role for: ${whatsapp_number}`);
        const agency = await Agency.query('agency_admin_phone_number').eq(whatsapp_number).exec();
        console.log(agency);
        if(!agency || agency.length < 1) {
            return null;
        }
        const userAgency = agency.map(a => a.toJSON())
            .filter(a => a.agency_admin_phone_number === whatsapp_number);
        return userAgency[0];
    } catch(err) {
        console.error(err);
        return null;
    }

}
async function addLogEntry({ user_whatsapp_number, message }) {
    try {
        await WhatsAppLog.create({
            log_id: uuid.v4(),
            creation_date_time: new Date(Date.now()).toISOString(),
            user_whatsapp_number,
            message
        });
    } catch(e) {
        console.log('[x] Error while creating log entry');
        console.error(e);
    }
}

module.exports = function({ S3, express, Aws, Sqs }) {

    const Handlers = require('./handlers')({ Company, Employee, TaskRequest, Request, createAnEmployee, Quota, Aws });

    // Helpers
    async function Null() {
        return new Promise(res => res(null));
    }

    function doesMatchingKeywordsExists(keywords, tasks) {
        return tasks.filter(t => {
            return keywords.filter(k => t.keywords.indexOf(k) >= 0).length > 0;
        }).length > 0;
    }
    async function systemMessage(message, from) {
        // process messages with 
        const [ _, option ] = message.split(' ').map(t => t.trim());
        console.log(`Got System Message: ${message} for option ${option}`);
        const agencyJson = await checkUserRole(from);
        const { agency_id } = agencyJson;
        if(!agency_id) {
            return null;
        }
        const xmlResponse = new MessageResponse();
        let companies;
        switch(option) {
            case '900':
                // return a list of employees
                console.log('900: List employees');
                companies = await Company.query('agency_id').eq(agency_id).exec();
                if(!companies || companies.length < 1) {
                    return xmlResponse.message('No Companies Found'); // no companies
                }
                const ids = companies.map(c => c.toJSON().company_id);
                console.log(`Ids: ${ids.join(', ')}`);
                const employeesColl = await Employee.scan('company_id').in(ids).exec();
                if(!employeesColl || employeesColl.length < 1) {
                    return xmlResponse.message('No Employees Found'); // no employees found
                }
                console.log(employeesColl);
                xmlResponse.message(
                    templates.getEmployeesTemplate(employeesColl.map(e => e.toJSON()))
                );
                return xmlResponse;
            case '901':
                // start add new user flow
                console.log('901: New employee');
                return xmlResponse.message(templates.createEmployeeFillerTemplate());
            case '902':
                // start delete a user flow
                console.log('902: Delete employee');
                return xmlResponse.message(templates.deleteEmployeeFillerTemplate());
            case '903':
                // return a list of companies
                console.log('903: List companies');
                companies = await Company.query('agency_id').eq(agency_id).exec();
                if(!companies || companies.length < 1) {
                    return ; // no companies
                }
                xmlResponse.message(
                    templates.getCompaniesTemplate(companies.map(c => c.toJSON()))
                );
                return xmlResponse;
            case '904':
                // start add new employee flow
                console.log('904: New Company');
                return xmlResponse.message(templates.createCompanyFillerTemplate());
            case '905':
                // start delete a company flow
                console.log('905: Delete Company');
                return xmlResponse.message(templates.deleteCompanyFillerTemplate());
            case '906':
                // send app download link
                // generate link
                console.log('906: App link');
                return xmlResponse.message(templates.appDownloadLink());
            case '907':
                // send manual download link
                // generate link
                console.log('907: Manual Link');
                return xmlResponse.message(templates.manualDownloadLink());
            case '908':
                // set default company for tasks flag
                console.log('908: Set Default Company');
                return xmlResponse.message(templates.setDefaultCompany());
            case '999':
            default: 
                // return service menu
                console.log('999: System Menu');
                return xmlResponse.message(templates.systemMenu());
        }
    }

    async function helpMessage(message) {
        // process messages with H
        const xmlResponse = new MessageResponse();
        return xmlResponse.message(templates.helpMenu());
    }

    async function taskMessage(message, employee) {
        // process message with T
        const xmlResponse = new MessageResponse();
        const { company_id, employee_id } = employee;
        if(!Handlers.checkGetTaskQuota(employee)) {
            return xmlResponse.message(templates.taskLimitReachedTemplate());
        }
        const parts = message.split(' ')
            .filter(p => p !== 'T' && p !== ' ' && p !== '');
        if(parts.length < 1) {
            // malformed message
            return xmlResponse.message('Invalid keywords');
        }
        let tasks;
        
        if(parts.length > 0 && Number.isNaN(parseInt(parts[0]))) {
            // keywords
            console.log('Parts');
            console.log(parts);
            try {
                let query = Task.query('company_id')
                    .using('company_id-index').eq(company_id).and();
                parts.forEach((k, i, a) => {
                    if(i === 0) {
                        query = query.where('keywords').contains(k);
                    }
                    else query = query.or().contains(k);
                    
                });
                tasks = await query.exec();
                console.log(tasks);
            } catch(err) {
                throw err;
            }
            
            // map and return
            //return `Here are the tasks with keywords you wanted: \n${parts.join('\n')}`;
            if(tasks && tasks.length > 0) {
                // generate a S3 file link
                // add the S3 file link with other details for the items
                tasks = tasks.map(task => {
                    return {
                        ...task.toJSON(),
                        url: S3.getSignedUrl('getObject', {
                            Bucket: 'company-tasks',
                            Key: `${company_id}/${task.file_name}`,
                            Expires: 60 * 30 // 30 mins
                        })
                    };
                });
                console.log(tasks);
                // create a Request entry for each task
                await Handlers.createRequestBatch(
                    tasks.map(t => ({
                        user_whatsapp_number: employee.whatsapp_number,
                        task_id: t.task_id,
                        user_id: employee.employee_id,
                        keywords_used: parts,
                        has_match: doesMatchingKeywordsExists(parts, tasks) 
                    }))
                );
                templates.getTasksTemplate(tasks, xmlResponse);
                console.log(xmlResponse.toString());
                return xmlResponse;
            } else {
                return xmlResponse.message('No Tasks Found');
            }
            //return JSON.stringify(tasks);
        } else {
            // task number
            try {
                const taskNum = parseInt(parts[0]);
                tasks = await Task.query('company_id')
                    .eq(company_id).and()
                    .where('task_number').eq(taskNum).exec();
                    // add the url for the task file
                if(!tasks || tasks.length < 1) {
                    return xmlResponse.message('No Such Tasks Found');
                }
                tasks = tasks.map(task => {
                    return {
                        ...task.toJSON(),
                        url: S3.getSignedUrl('getObject', {
                            Bucket: 'company-tasks',
                            Key: `${company_id}/${task.file_name}`,
                            Expires: 60 * 30 // 30 mins
                        })
                    };
                });
                templates.getTasksTemplate(tasks, xmlResponse);
                // create a Request entry for each task
                await Handlers.createRequestBatch(
                    tasks.map(t => ({
                        user_whatsapp_number: employee.whatsapp_number,
                        task_id: t.task_id,
                        user_id: employee.employee_id,
                        keywords_used: parts,
                        has_match: doesMatchingKeywordsExists(parts, tasks) 
                    }))
                );

                return xmlResponse;
            } catch(ParseError) {
                // malformed task number
                console.error(ParseError);
                return null;
            }
        }

    }

    async function listMessage() {
        // process message with L ; not List?
    }

    async function parseMessage({ message, to, from }) {
        const xmlResponse = new MessageResponse();
        let agency;
        if(message && message.length < 1) {
            return Null();
        }
        const startsWith = message.substr(0, 1).trim().toLocaleUpperCase();
        if(startsWith === 'S') {
            return systemMessage(message, from);
        } else if(startsWith === 'H') {
            return helpMessage(message);
        } else if(startsWith === 'L') {
            // LT messages
            console.log('LT message');
            const [ _, short_code, ...keywords ] = message.split(' ');
            try {
                // get company ID
                // get all tasks for company with keywords
                if(keywords.length < 1) {
                    return xmlResponse.message('Bad or no keywords given');
                }
                const company = await Company.query('short_code')
                    .eq(short_code.toLocaleUpperCase()).limit(1).exec();
                if(!company || company.length < 1) {
                    return xmlResponse.message('No Such Company exists');
                }
                const companyJson = company[0].toJSON();
                // this will be agency_admin
                let agencyAdmin = await Agency.query('agency_admin_phone_number')
                    .eq(from).exec();
                if(!agencyAdmin || agencyAdmin.length < 1) {
                    return xmlResponse.message('No such user exists');
                } else {
                    agencyAdmin = agencyAdmin[0].toJSON();
                }
                // need to remove task from here
                // at this point, the task does not exists
                // this will start the task running process
                // front end will execute the saved query
                // and generate task file and create task
                // task trigger will run, remove the SQS entry
                // and finally, send the task file on whatsapp
                // queue and send back a request ID
                // create SQS task
                const taskReq = await Handlers.createTaskRequest(agencyAdmin, companyJson, keywords);
                await Handlers.queueTasks(taskReq.toJSON(), Sqs);
                
                // send a message to notify task has been queued
                //templates.getTasksTemplate(tasks, xmlResponse);
                //console.log(xmlResponse.toString());
                const temp = templates.queuedTaskTemplate(taskReq);
                return xmlResponse.message(temp);
                

            } catch(err) {
                console.error(err);
            }
        } else if(startsWith === 'T') {
            // get company bucket name
            try {
                const employee = await Employee.query('whatsapp_number')    
                    .eq(from).exec();
                if(!employee || employee.length < 1) {
                    return xmlResponse.message('No such employee exists');
                }
                return taskMessage(message, employee[0].toJSON());
            } catch(err) {
                console.log('Emp Query error');
                throw err;
            }
        } else if(message.startsWith('<Create:employee>')) {
            agency = await checkUserRole(from);
            if(!agency) {
                return xmlResponse.message('Not Authorized');
            }
            const employee = await Handlers.createEmployee(message, agency);
            if(!employee || employee === 'QuotaExceeded') {
                if(employee === 'QuotaExceeded') {
                    return xmlResponse.message(templates.createQuotaErrorTemplate('Employee'))
                }
                return xmlResponse.message(templates.createErrorTemplate('Employee'));
            }
            return xmlResponse.message(templates.getEmployeesTemplate([ employee.toJSON() ]));
        } else if(message.startsWith('<Delete:company>')) {
            agency = await checkUserRole(from);
            if(!agency) {
                return xmlResponse.message('Not Authorized');
            }
            const deleted = await Handlers.deleteCompany(message);
            if(!deleted) {
                return xmlResponse.message(templates.deleteErrorTemplate('Company'));
            }
            return xmlResponse.message(templates.deletedTemplate('Company'));
        } else if(message.startsWith('<Delete:employee>')) {
            agency = await checkUserRole(from);
            if(!agency) {
                return xmlResponse.message('Not Authorized');
            }
            const deleted = await Handlers.deleteEmployee(message);
            if(!deleted) {
                return xmlResponse.message(templates.deleteErrorTemplate('Employee'));
            }
            return xmlResponse.message(templates.deletedTemplate('Employee'));
        } else if(message.startsWith('<Create:company>')) {
            agency = await checkUserRole(from);
            if(!agency) {
                return xmlResponse.message('Not Authorized');
            }
            const company = await Handlers.createCompany(message, agency);
            if(!company || company === 'QuotaExceeded') {
                if(company === 'QuotaExceeded') {
                    return xmlResponse.message(templates.createQuotaErrorTemplate('Company'));
                }
                return xmlResponse.message(templates.createErrorTemplate('Company'));
            }
            return xmlResponse.message(templates.getCompaniesTemplate([ company.toJSON() ]));
        } else {
            return Null();
        }
    }

    function parseWhatsAppNumber(number) {
        return number.split(':')[1];
    }

    function parseReqBody(body) {
        const { From, To, Body, SmsStatus } = body;
        if(SmsStatus === 'received') {
            return { 
                from: parseWhatsAppNumber(From),
                to: parseWhatsAppNumber(To),
                message: Body
            };
        }
    }

    const router = express.Router({
        mergeParams: true
    });
    
    router.post('/incoming', async (req, res) => {
        console.log('Incoming message POST');
        console.log(req.body);

        const { from, to, message } = parseReqBody(req.body);
        // add log entry
        await addLogEntry({ message, user_whatsapp_number: from });
        try {
            const msgResponse = await parseMessage({ message, to, from });
            if(msgResponse === null) {
                return res.status(400); // not an error in server so 400
            }
            console.log(msgResponse.toString());
            res.set('Content-Type', 'application/xml');
            res.send(msgResponse.toString());
        } catch(err) {
            console.error(err);
            res.sendStatus(500);
        }
    });

    router.post('/admin/resetPassword', async (req, res) => {
        /**
         * Why is this here? Two straightforward reasons
         * ---------------------------------------------
         * 1. Needs an API Gateway interface that has no authorization requirement
         * 2. To save resource; deploying a separate function for one endpoint
         *  just seemed extravagant
         */
        console.log(req.body);
        const { whatsapp_number } = req.body;
        console.log(`User with ${whatsapp_number} requested password change`);
        if(!whatsapp_number) {
            return res.status(400).send('Invalid Request');
        }
        try {
            const reset = await resetAgencyAdminPassword({ whatsapp_number, Aws });
            if(!reset) {
                return res.status(500).send('Could not reset password');
            }
            return res.status(200).send('Password Reset');
        } catch(e) {
            console.error(e);
            return res.status(500).send('Internal Error');
        }
    });
    

    return router;
};
