const { async } = require('crypto-random-string');
const uuid = require('uuid');
const SqsHandler = require('./../sqs');

function getFieldsForCreateFns(message) {
    return message.split('\n')
        .filter(line => line.indexOf(':') > -1 && !line.startsWith('<'))
        .map(keyValue => {
            const [ key, value ] = keyValue.split(':');
            return { [key.trim()]: value.trim() };
        })
        .reduce((p, c) => ({ ...p, ...c }), {});
}
function getFieldsForDeleteFns(message) {
    const o = message.split('\n')
        .filter(line => line.indexOf(':') > -1 && !line.startsWith('<'))
        .map(keyValue => {
            const [ key, value ] = keyValue.split(':');
            return { [key.trim()]: value.trim() };
        })[0];
    if(o.company_id) {
        return o.company_id;
    } else {
        return o.employee_id;
    }
}
function parseRequest(req) {
    const {
        user_whatsapp_number, task_id,
        user_id, keywords_used, has_match,
        is_live, is_completed
    } = req;
    // check required properties
    if(!user_whatsapp_number || !task_id || !user_id || !keywords_used || (typeof has_match !== 'boolean')) {
        return null;
    }
    const request_id = uuid.v4();
    const creation_date_time = new Date().toUTCString();
    return {
        request_id,
        user_whatsapp_number,
        task_id,
        user_id,
        creation_date_time,
        keywords_used,
        has_match,
        is_live: is_live ? is_live : false,
        is_completed: is_completed ? is_completed : true,
        completion_date_time: creation_date_time
    };
}

module.exports = function({ Company, Employee, Request, TaskRequest, createAnEmployee, Quota, Aws }) {
    async function requestBatch(requests, mapped = false) {
        if(!mapped) {
            requests = requests.map(r => parseRequest(r))
                .filter(r => r !== null);
        }
        console.log('Requests');
        console.log(requests);
        return Request.batchPut(requests);
    }
    async function writePerTick(requests) {
        const unprocessed = await requestBatch(requests, true);
        if(!unprocessed) {
            throw new Error('Could not batch write requests');
        };
        if(unprocessed.length && unprocessed.length > 0) {
            return process.nextTick(writePerTick, unprocessed);
        }
    }
    async function createRequest(req) {
        try {
            const parsed = parseRequest(req);
            if(!parsed) {
                throw new Error('Invalid Request');
            }
            const request = await Request.create(parsed);
            if(!request) {
                throw new Error('Could not create Request');
            }
            return { request: request.toJSON() };
        } catch(err) {
            throw err;
        }
    }
    
    return {
        createCompany: async (message, agency) => {
            const { companies_added, companies_quota_limit } = agency;
            if((companies_added + 1) > companies_quota_limit) {
                return 'QuotaExceeded';
            }
            const fields = getFieldsForCreateFns(message);
            fields.short_code = fields.short_code.toLocaleUpperCase();
            fields.company_id = uuid.v4();
            fields.employees_added = 0;
            console.log(fields);
            try {
                const company = await Company.create(fields);
                if(!company) {
                    return null;
                }
                return company;
            } catch(err) {
                throw err;
            }
        },
        createEmployee: async (message, agency) => {
            const { employees_quota_limit, employees_added } = agency;
            if((employees_added+1) > employees_quota_limit) {
                return 'QuotaExceeded';
            }
            const fields = getFieldsForCreateFns(message);
            fields.employee_id = uuid.v4();
            let email;
            if(fields.email) {
                email = fields.email;
                delete fields.email;
            }
            if(!email) {
                return null;
            }
            try {
                const employee = await Employee.create(fields);
                if(!employee) {
                    return null;
                }
                // create Cognito User
                const user = await createAnEmployee({
                    ...fields, email, Aws
                });
                if(!user) {
                    return null;
                }
                return employee;
            } catch(err) {
                throw err;
            }            
        },
        deleteCompany: async (message) => {
            const field = getFieldsForDeleteFns(message);
            try {
                await Company.delete(field);

                return true;
            } catch(err) {
                throw err;
            }
        },
        deleteEmployee: async (message) => {
            const field = getFieldsForDeleteFns(message);
            console.log(field);
            try {
                await Employee.delete(field);

                return true;
            } catch(err) {
                throw err;
            }
        },
        createRequestBatch: async (requests) => {
            /*try {
                const unprocessed = await requestBatch(requests);
                if(!unprocessed) {
                    throw new Error('Could not batch write requests');
                }
                if(unprocessed.length && unprocessed.length > 0) {
                    // start the recursive loop
                    return process.nextTick(writePerTick, unprocessed);
                }
            } catch(err) {
                throw err;
            }*/
            // create each request individually and then map
            // request_id to task_id
            const batch = [];
            for(const req of requests) {
                const { company_id } = req;
                delete req.company_id;
                const { request_id, task_id } = await createRequest(req);
                batch.push({ request_id, task_id, company_id });
            }
            return batch;
        },
        checkGetTaskQuota: async(employee) => {
            const { monthly_quota } = employee;
            if(monthly_quota <= 0) {
                return false;
            }
            return true;
        },
        queueTasks: async(req, sqs) => {
            // for each req task id pairs, create SQS entry
            const handler = SqsHandler({ Sqs: sqs }).queue;
            const { company_id, agency_id, creation_date_time, task_request_id } = req;
            try {
                await handler({
                    company_id, agency_id, task_request_id,
                    update_time: creation_date_time,
                    is_live: true
                });
            } catch(e) {
                throw e;
            }
        },
        createTaskRequest: async(agencyAdmin, company, keywords) => {
            try {
                const taskReq = await TaskRequest.create({
                    task_request_id: uuid.v4(),
                    user_whatsapp_number: agencyAdmin.agency_admin_phone_number,
                    company_id: company.company_id,
                    agency_id: agencyAdmin.agency_id,
                    creation_date_time: new Date().toISOString(),
                    keywords_used: keywords,
                    is_live: true,
                    is_completed: false,
                    completed_date_time: 'N/A'
                });
                if(!taskReq) {
                    return null;
                }
                return taskReq;
            } catch(err) {
                throw err;
            }
        }
    };
};