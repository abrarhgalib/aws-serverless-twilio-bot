/**
 * Lambda Authorizer implementation
 * for Lambda Proxy API Gateway
 * --------------------------------
 * Method ARN points to the API resource; not exactly a URL
 * Format: arn:aws:execute-api:region:account-id:api-id/stage-name/$connect
 */

const aws = require('aws-sdk');
const TokenValidator = require('cognito-express');
const ApiPermissions = require('./permissions.json');
const { Company } = require('./../models/Company');

// Helpers
function formatArn(arn = '') {
    const arnSplit = arn.split(':');
    const apiGatewaySplit = arnSplit[5].split('/');
    const region = arnSplit[3];
    const accountId = arnSplit[4];
    const [ apiId, stageName, connect ] = apiGatewaySplit;

    return {
        region, accountId, apiId, stageName, connect
    };
}
async function isAgencyCompany({ aid, cid, isShort }) {
    try {
        let company;
        if(isShort) {
            company = await Company.query('short_code')
                .eq(`${cid}`.toLocaleUpperCase()).limit(1).exec();
            if(!company || company.length < 1) {
                console.log('No Such Company Code');
                return false;
            }
            company = company[0];
        }
        else {
            company = await Company.get(cid);
            if(!company) {
                console.log('No Such Company');
                return false;
            }
        }
        const companyData = company.toJSON();
        if(companyData.agency_id !== aid) {
            return false;
        }
        return true;
    } catch(e) {
        console.error(e);
        return false;
    }
}
async function getUser(cognito, token) {
    try {
        const user = await cognito.getUser({ AccessToken: token }).promise();
        return {
            username: user.Username,
            attributes: user.UserAttributes
        };
    } catch(err) {
        console.error(err);
        return null;
    }
}
function generateArn({ methodArn, httpMethod, requestContext }) {
    const { apiId, region, accountId, stageName, connect } = formatArn(methodArn);
    //return `arn:aws:execute-api:${region}:${accountId}:${apiId}/${stageName}/${httpMethod}/`
    return methodArn;
}
function generatePolicy({ httpMethod, methodArn, requestContext, allow, principalId, number, entity_id, role }) {
    // returns a policy to allow/deny access
    const arn = generateArn({ methodArn, httpMethod, requestContext });
    console.log(`ARN: ${arn}`);
    console.log(number, entity_id, role);
    return {
        principalId: principalId,
        policyDocument: {
            Version: "2012-10-17",
            Statement: [
                {
                    Action: "execute-api:Invoke",
                    Effect: allow ? "Allow" : "Deny",
                    Resource: arn
                }
            ]
        },
        context: {
            // used for passing value to downstream lambda
            entity_id: entity_id,
            whatsapp_number: number,
            role: role
        }
    };
}

// Errors
class AccessDeniedError extends Error {
    constructor(message = 'Access Denied') {
        super(message);
        this.name = 'AccessDenied';
    }
}
class AuthError extends Error {
    constructor(message = 'Auth Error') {
        super(message);
        this.name = 'AuthError';
    }
}

exports.Authorize = (event, context, callback) => {
    console.log('Authorizing');
    const stage = process.env.STAGE ? process.env.STAGE : 'dev';
    const { httpMethod, headers, pathParameters, requestContext, methodArn } = event;
    console.log(context);
    console.log(event);
    //const { principalId } = context.authorizer; // undefined
    const path = requestContext.path;
    if(path.startsWith(`/${stage}/messages`)) {
        // no need go farther; consider as webhook request
        return callback(null, generatePolicy({
            httpMethod, methodArn, requestContext, principalId,
            allow: true
        }));
    }
    const cognito = new aws.CognitoIdentityServiceProvider();
    const tokenValidator = new TokenValidator({
        region: 'eu-central-1',
        cognitoUserPoolId: 'eu-central-1_tGV4WJfR6',
        tokenUse: 'access', // can be id as well
        tokenExpiration: 3600000
    });
    const {
        region, accountId, apiId, stageName, connect
    } = formatArn(methodArn);

    console.log(JSON.stringify({
        region, accountId, apiId, stageName, connect,
        pathParameters, requestContext, httpMethod,
        
    }));
    const idToken = headers['x-cognito-id-token'];
    const accessToken = headers['x-cognito-access-token'];

    console.log(idToken);
    console.log(accessToken);
    
    let number; 
    let role;
    let company_id;
    let _sub;
    let principalId;
    let policy;
    getUser(cognito, accessToken)
        .then(({ username, attributes }) => {
            if(!username && !attributes) {
                throw new AuthError();
            }
            _sub = username;
            attributes.forEach(a => {
                if(a.Name === 'phone_number') {
                    number = a.Value;
                } else if(a.Name === 'custom:role') {
                    role = a.Value;
                } else if(a.Name === 'custom:company_id') {
                    company_id = a.Value;
                } else if(a.Name === 'cognito:username') {
                    console.log(`CognitoUsername: ${a.Value}`);
                    principalId = a.Value;
                } else if(a.Name === 'sub') {
                    console.log(`Sub: ${a.Value}`);
                    principalId = a.Value;
                }
            });
            
            // decode token
            return tokenValidator.validate(accessToken);
        })
        .then(decoded => {
            // structure here: https://docs.aws.amazon.com/cognito/latest/developerguide/amazon-cognito-user-pools-using-tokens-with-identity-providers.html
            // username === sub
            console.log('Validated Access Token');
            const { sub } = decoded;
            if(_sub !== sub) {
                // invalid token
                throw new AccessDeniedError();
            }
            // check role for route
            // match against permissions for path and request methods
            const permissions = ApiPermissions[role];
            console.log(permissions);
            if(!permissions && role === 'admin') {
                // allow
                policy = generatePolicy({
                    httpMethod, methodArn, requestContext, 
                    principalId, number, role,
                    allow: true,
                    entity_id: company_id
                    
                });
                console.log('Returning allow policy');
                console.log(policy);
                return callback(null, policy);
            }
            if(!permissions) {
                // unknown role! [BUG]
                throw new AuthError('Unknown Role');
            }
            let end = path.replace('/dev/api', '');
            if(end.startsWith('/dev')) {
                // /dev/admin route
                end = path.replace('/dev/admin', '');
            }
            console.log(`End: ${end}`);
            const resource = permissions.find(perm => end.startsWith(perm.path));
            if(!resource) {
                // unknown path; reject
                console.log(`Unknown resource path: ${end}`);
                throw new AccessDeniedError();
            }
            if(typeof resource.allowedMethods === 'string' && resource.allowedMethods === '*') {
                // allow
                console.log('Returning allow policy for *');
                policy = generatePolicy({
                    httpMethod, methodArn, requestContext, 
                    principalId, number, role,
                    allow: true,
                    entity_id: company_id
                });
                console.log(policy);
                return callback(null, policy);                
            } else {
                if(resource.allowedMethods.indexOf(httpMethod) < 0) {
                    // reject
                    console.log(resource);
                    console.log(httpMethod);
                    throw new AccessDeniedError();
                } else {
                    // allow
                    if(resource.requiredParam) {
                        // check if company/agency ID match
                        if(['/db_configs', '/queries', '/employees', '/companies', '/agency', '/company/short_code'].indexOf(resource.path) > -1) {
                            if(end === '/companies/tasks/all') { // for admin only so deny
                                console.log('Returning deny policy');
                                policy = generatePolicy({
                                    httpMethod, methodArn, requestContext, 
                                    principalId, number, role,
                                    allow: false,
                                    entity_id: company_id
                                });
                                return callback(null, policy);
                            } else {
                                const cid = end.split('/').pop(); 
                                let isShort = false;
                                if(end.startsWith('/company/short_code')) {
                                    isShort = true;
                                    console.log(`Company Short code: ${cid}`);
                                }
                                if(cid === company_id) {
                                    console.log(`Cid: ${cid}  CompanyID: ${company_id}  Same?:${cid===company_id}`);
                                    console.log('Returning allow policy');
                                    policy = generatePolicy({
                                        httpMethod, methodArn, requestContext, 
                                        principalId, number, role,
                                        allow: true,
                                        entity_id: company_id
                                    });
                                    return callback(null, policy);            
                                } else {
                                    if(role === 'agency_admin') {
                                        // allow
                                        // add check to see if the company belongs to this agency
                                        isAgencyCompany({
                                            cid,
                                            isShort,
                                            aid: company_id // this is agency_id for agency_admin
                                        })
                                        .then(shouldAllow => {
                                            if(shouldAllow) {
                                                console.log('Returning allow policy');
                                                policy = generatePolicy({
                                                    httpMethod, methodArn, requestContext, 
                                                    principalId, number, role,
                                                    allow: true,
                                                    entity_id: company_id
                                                });
                                                return callback(null, policy);
                                            } else {
                                                console.log('Returning deny policy');
                                                policy = generatePolicy({
                                                    httpMethod, methodArn, requestContext, 
                                                    principalId, number, role,
                                                    allow: false,
                                                    entity_id: company_id
                                                });
                                                return callback(null, policy);
                                            }
                                        });
                                    } else {
                                        console.log('Returning deny policy');
                                        policy = generatePolicy({
                                            httpMethod, methodArn, requestContext, 
                                            principalId, number, role,
                                            allow: false,
                                            entity_id: company_id
                                        });
                                        return callback(null, policy);
                                    }
                                }
                            }
                        } else {
                            // unknown resource 
                            throw AccessDeniedError();
                        }
                    } else {
                        console.log('No required parameters');
                        console.log('Returning allow policy');
                        policy = generatePolicy({
                            httpMethod, methodArn, requestContext, 
                            principalId, number, role,
                            allow: true,
                            entity_id: company_id
                        });
                        console.log(policy);
                        return callback(null, policy);
                    }
                }
            }
            // at this point all checks passed
            // cache the request at Gateway and allow policy
        })
        .catch(e => {
            // auth error
            console.error(e);
            switch(e.name) {
                case 'AccessDenied':
                    callback(null, generatePolicy({ httpMethod, methodArn, requestContext, allow: false, principalId }));
                case 'AuthError':
                    // return an error
                    callback(null, generatePolicy({ httpMethod, methodArn, requestContext, allow: false, principalId }));
            }
        });
};