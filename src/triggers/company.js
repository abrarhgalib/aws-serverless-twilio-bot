const { Company } = require('./../models/Company');
const { Agency } = require('./../models/Agency');

module.exports = function({ Aws, Sns }) {
    return {
        companyCreated: async (record) => {
            // adjust monthly quota
            try {
                const { agency_id } = record.dynamodb.NewImage;
                // increment
                console.log(`AgencyID: ${agency_id.S}`);
                const updated = await Agency.update({ agency_id: agency_id.S }, { $ADD: { companies_added: 1 } });
                if(!updated) {
                    throw new Error('Could not update agency company count');
                }
                return true;
            } catch(e) {
                console.error(e);
                return false;
            }
        },
    };
};

