/**
 * Triggers for user_requests table
 * --------------------------------
 * 1. Adjust the monthly_quota for employee after task access
 */

const { Employee } = require('./../models/Employee');

module.exports = function({ Aws, Sns }) {
    return {
        userRequestCreated: async (record) => {
            // adjust monthly quota
            const { user_id } = record.dynamodb.NewImage;
            try {
                const updated = await Employee.update({ employee_id: user_id.S }, { $ADD: { monthly_quota: -1 } });
                if(!updated) {
                    return false;
                }
                return true;
            } catch(e) {
                console.error(e);
                return false;
            }
        },
    };
};

