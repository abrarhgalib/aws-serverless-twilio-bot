/**
 * Triggers for employees table
 * --------------------------------
 * 1. Adjust the company and agency employee count
 */
const dynamoOrm = require('dynamoose');
const { Company } = require('./../models/Company');
const { Agency } = require('./../models/Agency');

module.exports = function({ Aws, Sns }) {
    return {
        employeeCreated: async (company_id, total_added) => {
            // adjust monthly quota
            try {
                const company = await Company.get(company_id);
                if(!company) {
                    throw new Error('Company not found');
                }
                const { agency_id } = company.toJSON();
                // increment
                const companyUpdated = await Company.update({ company_id: company_id }, { $ADD: { employees_added: total_added } });
                if(!companyUpdated) {
                    throw new Error('Could not update company employee count');
                }
                const agencyUpdated = await Agency.update({ agency_id: agency_id }, { $ADD: { employees_added: total_added } });
                if(!agencyUpdated) {
                    throw new Error('Could not update agency employee count');
                }
                return true;
            } catch(e) {
                console.error(e);
                return false;
            }
        },
    };
};

