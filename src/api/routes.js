const multer = require('multer');
const multerS3 = require('multer-s3');
const uuid = require('uuid'); 

const { Company } = require('./../models/Company');
const { Employee, createAnEmployee } = require('./../models/Employee');
const { Task } = require('./../models/Task');
const { Query } = require('./../models/Query');
const { AgencyExecution, EAgencyExecutionLogTypes } = require('./../models/AgentExecution');
const { DbConfig } = require('./../models/DbConfig');
const { Request } = require('./../models/Request');
const { Quota } = require('./../models/Quota');
const { Agency } = require('./../models/Agency');
const { Log } = require('./../models/Log');
const { TaskRequest } = require('./../models/TaskRequest');

const Handlers = require('./handlers')({ Quota, Employee, Agency, Company, Task });

function getCompany(req) {
    const { company_name, agency_id, short_code, tax_id, billing_address, mailing_address } = req.body;
    if(!company_name || !agency_id || !short_code) {
        return null;
    }
    const company_id = uuid.v4();
    return {
        company_id, company_name, agency_id,
        short_code: `${short_code}`.toLocaleUpperCase(),
        tax_id: tax_id || 'N/A',
        billing_address: billing_address || 'N/A',
        mailing_address: mailing_address || 'N/A'
    };
}
function getEmployee(req) {
    const { 
        first_name, last_name, whatsapp_number,
        company_id, email
    } = req.body;
    if(!first_name || !last_name || !whatsapp_number || !company_id || !email) {
        return null;
    }
    const employee_id = uuid.v4();
    return {
        employee_id, 
        whatsapp_number, company_id, email,
        monthly_quota: 5,
        first_name: first_name.toLocaleLowerCase(),
        last_name: last_name.toLocaleLowerCase()
    };
}
function getTask(req) {
    let {
        keywords, company_id, name, task_number,
        query_id, frequency, task_request_id,
        last_status
    } = req.body;
    if(!keywords || !company_id || !name || !query_id || !frequency || !task_request_id) {
        return null;
    }
    const task_id = uuid.v4();
    if(typeof task_number !== 'number') {
        task_number = parseInt(task_number);
    }
    if(typeof frequency !== 'number') {
        frequency = parseInt(frequency);
    }
    if(task_number === NaN || frequency === NaN) {
        return null;
    }
    return {
        task_id, company_id, name,
        frequency, query_id, task_request_id,
        keywords: keywords,
        last_status: last_status || 'N/A',
        task_number: task_number || 0
    };
}
function getScheduledTask(req) {
    const d = new Date().toISOString();
    req.task_request_id = `Scheduled-${d}`;
    const t = getTask(req);
    t.file_name = 'N/A';
    t.file_mime_type = 'N/A';
    return t;
}
function getQuery(req) {
    const {
        company_id, db_config_id,
        query_name, query_string,
        last_status
    } = req.body;
    if(!company_id || !db_config_id || !query_name || !query_string) {
        return null;
    }
    const query_id = uuid.v4();
    return {
        query_id, company_id, db_config_id,
        query_name, query_string,
        last_status: last_status || 'N/A'
    };
}
function getDbConfig(req) {
    const {
        company_id, db_name, database_type,
        db_connection_url, db_connection_string, last_status
    } = req.body;
    if(!company_id || !db_name || !db_connection_url || !db_connection_string) {
        return null;
    }
    const db_config_id = uuid.v4();
    return {
        db_config_id, company_id,
        db_name, db_connection_url, db_connection_string,
        last_status: last_status || 'N/A',
        database_type: database_type || 'N/A'
    };
}

module.exports = function({ Aws, S3, notifier, express }) {
    const fileUploader = multer({
        storage: multerS3({
            s3: S3,
            bucket: 'company-tasks',
            key: async (req, file, cb) => {
                let { company_id } = req.body;
                if(!company_id) {
                    const { task_id } = req.params;
                    const task = await Task.get(task_id);
                    if(!task) {
                        cb(new Error('No Such Task'), null);
                    } else {
                        company_id = task.toJSON().company_id;
                    }
                }
                const path = `${company_id}/${file.originalname}`;
                cb(null, path);
            }
        })
    });

    const router = express.Router({
        mergeParams: true
    });

    /**
     * Access context here
     */
    router.all('/*', async (req, res, nxt) => {
        const context = req.apiGateway.context;
        const { authorizer } = req.apiGateway.event.requestContext;
        const { role, entity_id, whatsapp_number } = authorizer; // entity_id can be agency or company ID
        if(role === 'admin') {
            // don't log
            return nxt();
        }
        // add log entry here
        await Log.create({
            log_id: uuid.v4(),
            user_whatsapp_number: whatsapp_number,
            creation_date_time: new Date(Date.now()).toISOString(),
            entity_id: entity_id,
            user_role: role,
            request_endpoint: `${req.method}: ${req.path}` 
        });

        return nxt(); 
    });

    /*
        * Get the details of an individual company
        * ----------------------------------------
        * Method: GET
        * Parameters: company_id
        * On Success: 200 { company }
        * Roles: [ admin, agency_admin ]
    */
    router.get('/company/:company_id', async (req, res) => {
        const company_id = req.params.company_id;
        console.log('Getting company with ID: ' + company_id);
        if(!company_id) {
            return res.status(400).send('Invalid Request');
        }
        try {
            const company = await Company.get(company_id);
            if(!company) {
                return res.status(404).send('No Such Company');
            }
            return res.status(200).json({
                company: company.toJSON()
            });
        } catch(err) {
            console.error(err);
            return res.status(500).send('Internal Error');
        }
    });
    /*
        * Get the details of an individual company using short_code
        * Useful for short code name checking
        * ----------------------------------------
        * Method: GET
        * Parameters: short_code
        * On Success: 200 { company }
        * Roles: [ admin, agency_admin ]
    */
    
    router.get('/company/short_code/:short_code', async (req, res) => {
        const { short_code }= req.params;
        if(!short_code) {
            return res.status(400).send('Bad Request');
        }
        try {
            const company = await Company.query('short_code')
                    .eq(`${short_code}`.toLocaleUpperCase()).limit(1).exec();
            if(!company || company.length < 1) {
                return res.status(404).send('No Such Company');
            }
            return res.status(200).json({ company: company[0].toJSON() }); 
        } catch(e) {
            console.log('[x] Error getting company with short_code');
            console.error(e);
            return res.status(500).send('Internal Error');
        }
    });
    /*
        * Adds a company
        * --------------
        * Method: PUT
        * Body: [ company_name, agency_id ]
        * On Success: 201 { company }
        * Roles: [ admin, agency_admin ]
    */
    router.put('/company', Handlers.checkCompaniesAddedQuota, Handlers.checkIfShortCodeExists, async (req, res) => {
        try {
            const c = getCompany(req);
            if(!c) {
                return res.status(400).send('Invalid Request');
            }
            const company = await Company.create(c);
            if(!company) {
                return res.status(500).send('Could not create company');
            }
            return res.status(201).json({
                company: company.toJSON()
            });
        } catch(err) {
            console.error(err);
            return res.status(500).send('Internal Error');
        }
    });
    /*
        * Deletes a company
        * -----------------
        * Method: DELETE
        * Parameters: company_id
        * Roles: [ admin, agency_admin ]
    */
    router.delete('/company/:company_id', async (req, res) => {
        const { company_id } = req.params;
        if(!company_id) {
            return res.status(400).send('Invalid Request');
        }
        try {
            await Company.delete(company_id);

            return res.status(200).send('Company Deleted');
        } catch(err) {
            console.error(err);
            return res.status(500).send('Internal Error');
        }
    });

    /*
        * Get an employee
        * ---------------
        * Method: GET
        * Parameters: employee_id
        * On Success: 200 { employee }
        * Roles: [ admin, agency_admin, employee ]
    */
    router.get('/employee/:employee_id', async (req, res) => {
        const employee_id = req.params.employee_id;
        if(!employee_id) {
            return res.status(400).send('Invalid Request');
        }
        try {
            const employee = await Employee.get(employee_id);
            if(!employee) {
                return res.status(404).send('No Such Employee');
            }
            return res.status(200).json({
                status: 200,
                employee: employee.toJSON()
            });
        } catch(err) {
            console.error(err);
            return res.status(500).send('Internal Error');
        }
    });
    /*
        * Adds an employee to a company
        * -----------------------------
        * Method: PUT
        * Body: [ 
        *   first_name, last_name, whatsapp_number,
        *   company_id, monthly_quota? 
        * ]
        * On Success: 201 { employee }
        * Roles: [ admin, agency_admin ]
    */
    router.put('/employee', Handlers.checkEmployeesAddedQuota, async (req, res) => {
        try {
            const e = getEmployee(req);
            const email = e.email;
            delete e.email;
            if(!e) {
                return res.status(400).send('Invalid Request');
            }
            const employee = await Employee.create(e);
            if(!employee) {
                return res.status(500).send('Could not create employee');
            }
            // create cognito user account
            const cognitoAccount = await createAnEmployee({ ...e, email, Aws });
            if(!cognitoAccount) {
                return res.status(500).send('Could not create employee');
            }
            const { username, attributes } = cognitoAccount;
            const role = attributes['custom:role'];
            return res.status(201).json({
                employee: {
                    ...employee.toJSON(),
                    username,
                    role
                }
            });
        } catch(err) {
            console.error(err);
            return res.status(500).send('Internal Error');
        }
    });
    /*
        * Deletes an employee
        * -------------------
        * Method: DELETE
        * Parameters: employee_id
        * On Success: 200 Employee Deleted
        * Roles: [ admin, agency_admin ]
    */
    router.delete('/employee/:employee_id', async (req, res) => {
        const { employee_id } = req.params;
        if(!employee_id) {
            return res.status(400).send('Invalid Request');
        }
        try {
            await Employee.delete(employee_id);

            return res.status(200).send('Employee Deleted');
        } catch(err) {
            console.error(err);
            return res.status(500).send('Internal Error');
        }
    });

    /*
        * Get a task
        * ----------
        * Method: GET
        * Parameters: task_id
        * On Success: 200 { task }
        * Roles: [ admin, agency_admin ]
    */
    router.get('/task/:task_id', async (req, res) => {
        const task_id = req.params.task_id;
        if(!task_id) {
            return res.status(400).send('Invalid Request');
        }
        try {
            const task = await Task.get(task_id);
            if(!task) {
                return res.status(500).send('Could not create task');
            }
            return res.status(200).json({
                task: task.toJSON()
            });
        } catch(err) {
            console.error(err);
            return res.status(500).send('Internal Error');
        }
    });
    /*
        * Adds a task for a company
        * -------------------------
        * Method: PUT
        * Body: [ keywords, company_id, name, task_number, file ]
        * On Success: 201 { task }
        * Roles: [ admin, agency_admin ]
    */
    router.put('/task', fileUploader.single('file'), async (req, res) => {
        const file = req.file;
        const file_name = file.originalname;
        try {
            // create the task
            const t = getTask(req);
            if(!t) {
                // delete from S3
                return res.status(400).send('Invalid Request');
            }
            t.file_name = file_name;
            t.file_mime_type = file.mimetype;
            const task = await Task.create(t);
            if(!task) {
                return res.status(500).send('Could not create task');
            }
            // remove task from queue => not possible since ReceiptHandle is only
            // available to message consumer
            // update the task_request as not live and complete
            if(!t.task_request_id.startsWith('Scheduled')) {
                // there no task request for this so skip
                await TaskRequest.update({ task_request_id: t.task_request_id }, {
                    is_live: false,
                    is_completed: true,  
                    completed_date_time: new Date().toISOString()
                });
            }
            return res.status(201).json({
                task: task.toJSON()
            });
        } catch(err) {
            console.error(err);
            return res.status(500).send('Internal Error');
        }
    });
    /*
        * Updates a task for a company
        * -------------------------
        * Method: PUT
        * Body: [ is_live ]
        * On Success: 201 { task }
        * Roles: [ admin, agency_admin ]
    */
    router.post('/task/:task_id', async (req, res) => {
        const task_id = req.params.task_id;
        const { is_live } = req.body;
        if(!task_id || is_live === undefined) {
            return res.status(400).send('Bad Request');
        }
        try {
            const task = await Task.get(task_id);
            if(!task) {
                return res.status(404).send('No Such Task');
            }
            const { company_id } = task.toJSON();
            await Task.update({ 'task_id': task_id }, {
                is_live
            });

            await notifier({ 
                task_id, company_id, is_live, 
                update_time: new Date(Date.now()).toISOString() 
            })
            .then(data => {
                console.log(`Task ID: ${task_id} updated. Message: ${data.MessageId}`);
            })
            .catch(e => {
                console.log('[x] Error while queueing notification');
                console.error(e);
            });

            return res.status(200).send('Task Updated');
        } catch(err) {
            console.error(err);
            return res.status(500).send('Internal Error');
        }
    });
    /*
        * Deletes a task
        * --------------
        * Method: DELETE
        * Parameters: task_id
        * On Success: 200 Task Deleted
        * Roles: [ admin, agency_admin ]
    */
    router.delete('/task/:task_id', async (req, res) => {
        const { task_id } = req.params;
        if(!task_id) {
            return res.status(400).send('Invalid Request');
        }
        try {
            const task = await Task.get(task_id);
            if(!task) {
                return res.status(404).send('No Such Task');
            }
            const taskJson = task.toJSON();
            const { company_id, file_name } = taskJson;
            
            // delete from S3 and then from DB
            const deleted = await S3.deleteObject({
                Bucket: 'company-tasks',
                Delete: [
                    { Key: `${company_id}/${file_name}` }
                ]
            }).promise();
            await Task.delete(task_id);

            return res.status(200).send('Task Deleted');
        } catch(err) {
            console.error(err);
            return res.status(500).send('Internal Error');
        }
    });
    
    /**
     * Create scheduled task
     * ---------------------
     * Method: PUT
     * On Success: 200, { task }
     * Roles: [ admin, agency_admin ] 
     */
    router.put('/scheduled_task', async (req, res) => {
        try {
            const sTask = getScheduledTask(req);
            if(!sTask) {
                return res.status(400).send('Invalid Request');
            }
            const task = await Task.create(sTask);
            if(!task) {
                return status(500).send('Could not create task');
            }
            return res.status(201).json({ task: task.toJSON() });
        } catch(e) {
            console.error(e);
            return res.status(500).send('Internal Error');
        }
    });

    /**
     * Updates a scheduled task with file
     * ---------------------------------
     * Method: POST
     * On Success: 200, { task }
     * Roles: [ admin, agency_admin ] 
     * 
     */
    router.post('/scheduled_task/:task_id', fileUploader.single('file'), async (req, res) => {
        try {
            const { task_id } = req.params;
            const file = req.file;
            const updates = {
                file_name: file.originalname,
                file_mime_type: file.mimetype
            };
            await Task.update({ task_id }, updates);

            return res.status(200).send('Task Updated');
        } catch(e) {
            console.error(e);
            return res.status(500).send('Internal Error');
        }
    });

    /**
     * Get a query
     * -----------
     * Method: GET
     * On Success: 200, { query }
     * Roles: [ admin, agency_admin ]
     */
    router.get('/query/:query_id', async (req, res) => {
        const { query_id } = req.params;
        if(!query_id) {
            return res.status(400).send('Invalid Request');
        }
        try {
            const query = await Query.get(query_id);
            if(!query) {
                return res.status(404).send('No Such Query');
            }
            return res.status(200).json({ query: query.toJSON() });
        } catch(err) {
            console.error(err);
            return res.status(500).send('Internal Error');
        }
    });
    /**
     * Creates a query
     * -----------
     * Method: PUT
     * On Success: 200, { query }
     * Roles: [ admin, agency_admin ]
     */
    router.put('/query', async (req, res) => {
        try {
            const query = getQuery(req);
            if(!query) {
                return res.status(400).send('Invalid Request');
            }
            const queryDoc = await Query.create(query);
            if(!queryDoc) {
                return res.status(500).send('Could not create query');
            }
            return res.status(201).json({ query: queryDoc.toJSON() });
        } catch(err) {
            console.error(err);
            return res.status(500).send('Internal Error');
        }
    });
    /**
     * Deletes a query
     * -----------
     * Method: DELETE
     * On Success: 200 Query Deleted
     * Roles: [ admin, agency_admin ]
     */
    router.delete('/query/:query_id', async (req, res) => {
        const { query_id } = req.params;
        if(!query_id) {
            return res.status(400).send('Invalid Request');
        }
        try {
            await Query.delete(query_id);

            return res.status(200).send('Query Deleted');
        } catch(err) {
            console.error(err);
            return res.status(500).send('Internal Error');
        }
    });
    /**
     * Gets a db config
     * -----------------
     * Method: GET
     * On Success: 200, { db_config }
     * Roles: [ admin, agency_admin ]
     */
    router.get('/db_config/:db_config_id', async (req, res) => {
        const { db_config_id } = req.params;
        if(!db_config_id) {
            return res.status(400).send('Invalid Request');
        }
        try {
            const db_config = await DbConfig.get(db_config_id);
            if(!db_config) {
                return res.status(404).send('No Such Db Config');
            }
            return res.status(200).json({ db_config: db_config.toJSON() });
        } catch(err) {
            console.error(err);
            return res.status(500).send('Internal Error');
        }
    });
    /**
     * Creates a db config
     * -----------------
     * Method: PUT
     * On Success: 200, { db_config }
     * Roles: [ admin, agency_admin ]
     */
    router.put('/db_config', async (req, res) => {
        try {
            const dbConfig = getDbConfig(req);
            if(!dbConfig) {
                return res.status(400).send('Invalid Request');
            }
            const db_config = await DbConfig.create(dbConfig);
            if(!db_config) {
                return res.status(500).send('Could not create DB Config');
            }
            return res.status(201).json({ db_config: db_config.toJSON() });
        } catch(err) {
            console.error(err);
            return res.status(500).send('Internal Error');
        }
    });
    /**
     * Deletes a db config
     * -----------------
     * Method: DELETE
     * On Success: 200 DB Config Deleted
     * Roles: [ admin, agency_admin ]
     */
    router.delete('/db_config/:db_config_id', async (req, res) => {
        const { db_config_id } = req.params;
        if(!db_config_id) {
            return res.status(400).send('Invalid Request');
        }
        try {
            await DbConfig.delete(db_config_id);

            return res.status(200).send('DB Config Deleted');
        } catch(err) {
            console.error(err);
            return res.status(500).send('Internal Error');
        }
    });
    

    /*
        * Gets all registered companies
        * -----------------------------
        * Method: GET
        * On Success: 200 { companies: [ { company }, ... ] }
        * Roles: [ admin ]
    */
    router.get('/companies', async (req, res) => {
        try {
            const companies = await Company.scanAll();
            if(!companies) {
                return res.status(500).send('Could not get all companies');
            }
            return res.status(200).json({ companies: companies.map(c => c.toJSON()) });
        } catch(err) {
            console.error(err);
            return res.status(500).send('Internal Error');
        }
    });
    /*
        * Gets all the employees from all companies
        * -----------------------------------------
        * Method: GET
        * On Success: 200 { employees: [ { employee }, ... ] }
        * Roles: [ admin ]
    */
    router.get('/employees', async (req, res) => {
        try {
            const employees = await Employee.scanAll();
            if(!employees) {
                return res.status(500).send('Could not get all employees');
            }
            return res.status(200).json({ employees: employees.map(e => e.toJSON()) });
        } catch(err) {
            console.error(err);
            return res.status(500).send('Internal Error');
        }
    });
    /*
        * Gets all companies under an agency
        * ----------------------------------
        * Method: GET
        * On Success: 200 { companies: [ { company }, ... ] }
        * Roles: [ admin, agency_admin ]
    */
    router.get('/companies/:agency_id', async (req, res) => {
        const { agency_id } = req.params;
        if(!agency_id) {
            return res.status(400).send('Invalid Request');
        }
        try {
            const companies = await Company.query('agency_id')
                .using('agency_id-index')
                .eq(agency_id).all().exec();
            if(!companies) {
                return res.status(500).send('Could not get companies');
            }
            if(companies.length < 1) {
                return res.status(404).send('No Companies For Agency');
            }
            return res.status(200).json({ companies: companies.map(c => c.toJSON()) });
        } catch(err) {
            console.error(err);
            return res.status(500).send('Internal Error');
        }
    });
    /*
        * Gets all employees for a particular company
        * -------------------------------------------
        * Method: GET
        * Parameters: company_id
        * On Success: 200 { employees: [ { employee }, ... ] }
        * Roles: [ admin, agency_admin ]
    */
    router.get('/employees/:company_id', async (req, res) => {
        const { company_id } = req.params;
        if(!company_id) {
            return res.status(400).send('Invalid Request');
        }
        try {
            const employees = await Employee.query('company_id').eq(company_id).exec();
            if(!employees) {
                return res.status(500).send('Could not get employees for company');
            }
            if(employees.length < 1) {
                return res.status(404).send('No Employees for company');
            }
            return res.status(200).json({ employees: employees.map(e => e.toJSON()) });
        } catch(err) {
            console.error(err);
            return res.status(500).send('Internal Error');
        }
    });
    /*
        * Get all tasks for all companies
        * -------------------------------
        * Method: GET
        * On Success: 200 { tasks: [ { task }, ... ] }
        * Roles: [ admin ]
    */
   router.get('/companies/tasks/all', async (req, res) => {
        try {
            const tasks = await Task.scanAll();
            console.log(tasks);
            if(!tasks) {
                return res.status(500).send('Could not get all tasks');
            }
            if(tasks.length < 1) {
                return res.status(404).send('No Tasks Found');
            }
            return res.status(200).json({ tasks: tasks.map(t => t.toJSON()) });
        } catch(err) {
            console.error(err);
            return res.status(500).send('Internal Error');
        }
   });
   /*
        * Gets all the tasks for a company
        * --------------------------------
        * Method: GET
        * Parameters: company_id
        * On Success: 200 { tasks: [ { task }, ... ] }
        * Roles: [ admin, agency_admin ]
   */
    router.get('/company/:company_id/tasks', async (req, res) => {
        const { company_id } = req.params;
        if(!company_id) {
            return res.status(400).send('Invalid Request');
        }
        try {
            const tasks = await Task.query('company_id').eq(company_id).exec();
            if(!tasks) {
                return res.status(500).send('Could not get tasks for company');
            }
            if(tasks.length < 1) {
                return res.status(404).send('No Tasks Found');
            }
            return res.status(200).json({ tasks: tasks.map(t => t.toJSON()) });
        } catch(err) {
            console.error(err);
            return res.status(500).send('Internal Error');
        }
    });
    /*
        * Gets all queries 
        * --------------------------------
        * Method: GET
        * On Success: 200 { queries: [ { query }, ... ] }
        * Roles: [ admin ]
   */
    router.get('/queries', async (req, res) => {
        try {
            const queries = await Query.scanAll();
            if(queries.length < 1) {
                return res.status(404).send('No Queries Found');
            }
            return res.status(200).json({ queries: queries.map(q => q.toJSON()) });
        } catch(err) {
            console.error(err);
            return res.status(500).send('Internal Error');
        }
    });
    /*
        * Gets all queries for a company
        * --------------------------------
        * Method: GET
        * Parameters: company_id
        * On Success: 200 { queries: [ { query }, ... ] }
        * Roles: [ admin, agency_admin ]
   */
    router.get('/queries/:company_id', async (req, res) => {
        const { company_id } = req.params;
        if(!company_id) {
            return res.status(400).send('Invalid Request');
        }
        try {
            const queries = await Query.query('company_id').eq(company_id).exec();
            if(!queries || queries.length < 1) {
                return res.status(404).send('No Queries Found');
            }
            return res.status(200).json({ queries: queries.map(q => q.toJSON()) });
        } catch(err) {
            console.error(err);
            return res.status(500).send('Internal Error');
        }
    });
    /*
        * Gets all db configs for a company
        * --------------------------------
        * Method: GET
        * Parameters: company_id
        * On Success: 200 { db_configs: [ { db_config }, ... ] }
        * Roles: [ admin, agency_admin ]
   */
    router.get('/db_configs/:company_id', async (req, res) => {
        const { company_id } = req.params;
        if(!company_id) {
            return res.status(400).send('Invalid Request');
        }
        try {
            const db_configs = await DbConfig.query('company_id').eq(company_id).exec();
            if(!db_configs || db_configs.length < 1) {
                return res.status(404).send('No DB Configs Found');
            }
            return res.status(200).json({ db_configs: db_configs.map(c => c.toJSON()) });
        } catch(err) {
            console.error(err);
            return res.status(500).send('Internal Error');
        }
    });
    /*
        * Gets all db configs
        * --------------------------------
        * Method: GET
        * On Success: 200 { db_configs: [ { db_config }, ... ] }
        * Roles: [ admin ]
   */
    router.get('/db_configs', async (req, res) => {
        try {
            const db_configs = await DbConfig.scanAll();
            if(!db_configs || db_configs.length < 1) {
                return res.status(404).send('No DB Configs Found');
            }
            return res.status(200).json({ db_configs: db_configs.map(c => c.toJSON()) });
        } catch(err) {
            console.error(err);
            return res.status(500).send('Internal Error');
        }
    });

    return router;
};