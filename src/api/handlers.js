module.exports = function({ 
    Quota, Employee, Company,
    Task, Agency
}) {
    return {
        checkEmployeesAddedQuota: async function(req, res, nxt) {
            if(!req.body.company_id) {
                return res.send(400).send('Bad Request');
            }
            try {
                const company = await Company.get(req.body.company_id);
                if(!company) {
                    return res.status(404).send('No Such Company');
                }
                const companyData = company.toJSON();
                const agency_id = companyData.agency_id;
                const agency = await Agency.get(agency_id);
                if(!agency) {
                    console.log('Agency for company not found; BUG');
                    return res.status(500).send('Fatal Error');
                }
                const agencyData = agency.toJSON();
                const num_employees = agencyData.employees_added;
                const agency_num_employees = agencyData.employees_quota_limit;
                if(num_employees >= agency_num_employees) {
                    return res.status(429).send('Quota Limit Exceeded');
                }
                return nxt();
            } catch(e) {
                console.error(e);
                return res.status(500).send('Internal Error');
            }
        },
        checkCompaniesAddedQuota: async function(req, res, nxt) {
            if(!req.body.agency_id) {
                return res.status(400).send('Bad Request');
            }
            try {
                const agency = await Agency.get(req.body.agency_id);
                if(!agency) {
                    return res.status(404).send('No Such Agency');
                }
                const agencyData = agency.toJSON();
                const num_companies = agencyData.companies_added;
                const companies_quota = agencyData.companies_quota_limit;
                if(num_companies >= companies_quota) {
                    return res.status(429).send('Quota Limit Exceeded');
                }
                return nxt();
            } catch(e) {
                console.error(e);
                return res.status(500).send('Internal Error');
            }
        },
        checkIfShortCodeExists: async function(req, res, nxt) {
            /**
             * Checks if a short_code exists in dynamoDB
             */
            const { short_code } = req.body;
            if(!short_code) {
                return nxt(); // will catch this error    
            }
            try {
                const company = await Company.query('short_code')
                    .eq(`${short_code}`.toLocaleUpperCase()).limit(1).exec();
                if(!company || company.length < 1) {
                    return nxt(); // does not exists
                } else {
                    return res.status(409).send('Short code already exists');
                }
            } catch(e) {
                console.log('[x] Error querying company with short_code');
                console.error(e);
                return res.status(500).send('Internal Error');
            }
        }
    };
};