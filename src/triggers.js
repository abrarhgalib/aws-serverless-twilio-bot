/**
 * Definitions for all deployed resource triggers
 * ----------------------------------------------
 */

const aws = require('aws-sdk');
const sns = new aws.SNS();

const EDbEvents = {
    INSERT: 'INSERT',
    MODIFY: 'MODIFY',
    REMOVE: 'REMOVE'
};

const TaskTriggers = require('./triggers/task')({ Aws: aws, Sns: sns });
const UserRequestTriggers = require('./triggers/user_request')({ Aws: aws, Sns: sns });
const EmployeeTriggers = require('./triggers/employee')({ Aws: aws, Sns: sns });
const CompanyTriggers = require('./triggers/company')({ Aws: aws });

/*
exports.taskChanged = (event, content, callback) => {
    const map = {
        created: [],
        modified: [],
        removed: []
    };
    event.Records.forEach(record => {
        console.log(`Stream record: ${JSON.stringify(record, null, 2)}`);

        if(event.type === EDbEvents.INSERTED) {
            map.created.push(record);      
        } else if(event.type === EDbEvents.MODIFY) {
            map.modified.push(record);
        } else {
            // deleted
            map.removed.push(record);
        }
    });
    Promise.all(
        Object.keys(map).map(type => {
            switch(type) {
                case 'created':
                    return TaskTriggers.tasksCreated(map[type]);
                case 'modified':
                    return TaskTriggers.tasksModified(map[type]);
                case 'removed':
                    return TaskTriggers.tasksRemoved(map[type]);
            }
        })
    ).then(() => {
        // finished trigger
    }).catch(err => {

    });
};


exports.agencyChanged = (event, context, callback) => {

};*/

exports.employeeChanged = (event, context, callback) => {
    const changes = {
        created: {},
    };
    event.Records.forEach(record => {
        if(record.eventName === EDbEvents.INSERT) {
            const { company_id } = record.dynamodb.NewImage;
            if(changes.created[company_id.S]) {
                changes.created[company_id.S].push(record);
            } else {
                changes.created[company_id.S] = [ record ];
            }
        }
    });
    console.log(changes);
    Promise.all(
        Object.keys(changes.created)
            .map(cid => {
                console.log(`CID: ${cid}`);
                const added = changes.created[cid].reduce(p => p + 1, 0);
                return EmployeeTriggers.employeeCreated(cid, added);
            })
    ).then(updates => {
        if(updates.filter(u => !u).length > 0) {
            console.log('Some updates failed');
        } else {
            console.log('All updates passed');
        }
        return callback(null, true);
    }).catch(e => {
        console.error(e);
        callback(e, null);
    });
};

exports.userRequestsChanged = (event, context, callback) => {
    const created = [];
    event.Records.forEach(record => {
        if(record.eventName === EDbEvents.INSERT) {
            created.push(record);
        }
    });
    Promise.all(
        created.map(r => UserRequestTriggers.userRequestCreated(r))
    ).then(updates => {
        if(updates.filter(u => !u).length < 1) {
            // all successful
            return callback(null, true);
        } else {
            throw new Error('Could not update quota');
        }
    }).catch(e => {
        console.error(e);
        callback(e, null);
    });
};

exports.companyChanged = (event, context, callback) => {
    const created = [];
    event.Records.forEach(record => {
        if(record.eventName === EDbEvents.INSERT) {
            created.push(record);
        }
    });
    Promise.all(
        created.map(r => CompanyTriggers.companyCreated(r))
    ).then(updates => {
        if(updates.filter(u => !u).length < 1) {
            return callback(null, true);
        } else {
            throw new Error('Could not update quota');
        }
    }).catch(e => {
        console.error(e);
        callback(e, null);
    });
};
